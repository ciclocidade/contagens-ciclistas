####################################
#### SOBRE OS DADOS DE CONTAGEM ####
####################################
# Fonte dos dados: http://www.cetsp.com.br/consultas/bicicleta/contadores-de-bicicletas.aspx
# Existem dois tipos de contagem automática: as fixas e as móveis.
# Existem 3 contadores automáticos fixos que funcionam desde 2016:
#     a) Faria Lima
#     b) Gastão Vidigal
#     c) Vergueiro
# Existem contagens pontuais em outros locais da cidade, feitas por equipamentos automáticos móveis. 
# Essas contagens duram 10 dias e informam os volumes por faixa horária.
#
# Por fim, existem contagens manuais feitas em algumas faixas horárias e logradouros específicos.
# Normalmente essas contagens são feitas com outros propósitos, mas acabam também contando bicicletas.

###########################################################################################
#### OBJETIVO DO CÓDIGO: Limpar e padronizar os bancos de dados de contagem automática ####
###########################################################################################
# 1) padronizar nomes de coluna -> feito manualmente no excel;
# 2) criar variáveis:
#   a) dia da semana
#   b) lat - long
# 3) limpezas
#   a) manter apenas dia de semana
#   b) excluir NA (acontecem apenas na Vergueiro e Faria Lima)
#   c) excluir zeros entre 6h e 21h.
# 4) unir todos os arquivos em um único banco de dado.

####################
#### DEFINIÇÕES ####
####################
# rm(list=ls())
# gc()
# options(stringsAsFactors = F)
# `%notin%` = Negate(`%in%`)
# select <- dplyr::select

#####################
#### BIBLIOTECAS ####
#####################
# load.lib <- c("tidyverse", "sf", "xlsx", "readxl")

# Carregando os pacotes e instalando algum se necessário
# install.lib <- load.lib[!load.lib %in% installed.packages()]
# for(lib in install.lib) install.packages(lib,dependencies=TRUE)
# sapply(load.lib, require, character=TRUE)
# 
# remove(install.lib, lib, load.lib)

###################
#### DIRETÓRIO ####
###################
# definindo a pasta para o projeto
# dir_path <- "C:/Users/taina/Dropbox/Trabalho/2021_ciclocidade_strava/"

# CRIAR SUBPASTAS:
# dados/input
# dados/intermediate
# dados/output

# figuras/intermediate
# figuras/results

##########################
#### ABRINDO OS DADOS ####
##########################
# listando arquivos salvos em padrão "xlsx" das contagens para ler
files_xlsx <- grep(
  list.files(
    path = paste0(dir_path, "dados/input/contagens/site_CET/"), 
    all.files = FALSE,
    full.names = TRUE
    ), 
  pattern="mov_|.csv|0_planilha_modelo|nomes_IDs",      # arquivos que não devem ser listados
  inv=TRUE, 
  value=TRUE)

#estava dando um problema com o arquivo da Inajar, ele puxa um "~$01_Av. Inajar de Souza.xlsx",
# como se houvesse um arquivo aberto. Já fechei tudo no meu PC, mas continua assim. 
# Se para você não der problema, então não precisa excluir essa primeira observação.
# vamos garantir que esse arquivo não está sendo incluído.
files_xlsx <- files_xlsx[files_xlsx != paste0(dir_path, "dados/input/contagens/site_CET/~$01_Av. Inajar de Souza.xlsx")]

# abrindo arquivos de contagens móveis
contagens_auto <- files_xlsx %>% 
  map_df(read_excel)

remove(files_xlsx)
####################################
#### TRANSFORMAÇÕES - CONTAGENS ####
####################################
contagens_auto <- contagens_auto %>% 
  mutate(DATA = as.Date(DATA, origin = "1899-12-30")) %>% 
  mutate(DIA_SEMANA = weekdays(DATA)) 

######################
#### VERIFICAÇÕES ####
######################
dim(contagens_auto)

#verificando NA
contagens_na <- contagens_auto %>% filter(is.na(TOTAL))
# table(contagens_na$DATA, contagens_na$ID_PTOS)
write.csv2(contagens_na, file = paste0(dir_path, "dados/intermediate/contagens_automaticas_NA.csv"))
remove(contagens_na)

#verificando zeros
contagens_zero <- contagens_auto %>% filter(TOTAL == 0)
# table(contagens_zero$FAIXA_HORA)
write.csv2(contagens_zero, file = paste0(dir_path, "dados/intermediate/contagens_automaticas_ZERO.csv"))
contagens_zero <- contagens_zero %>% filter(FAIXA_HORA > 4 & FAIXA_HORA < 23)
remove(contagens_zero)

# qual o primeiro e último dia da base? Preciso dessa informação para pedir os dados de chuva e temperatura
min(contagens_auto$DATA) # 20/01/2016
max(contagens_auto$DATA) # 15/06/2021 --> 04/10/2021

#################
#### LIMPEZA ####
#################
# 1. excluir finais de semana
# 2. excluir NA
# 3. excluir contagens zero entre 6h e 21h.
# 4. excluir feriados

# excluir finais de semana ----
contagens_auto <- contagens_auto %>%  
  filter(DIA_SEMANA %notin% c("sábado", "domingo"))

#excluir NA ----
# table(is.na(contagens_auto$TOTAL)) # ainda persistem 259 observações com NA
# contador que marcou zero no dia todo ou em boa parte do dia.
# somente na Vergueiro e na Faria Lima
contagens_na <- contagens_auto %>% filter(is.na(TOTAL))
# table(contagens_na$DATA, contagens_na$ID_PTOS)
write.csv2(contagens_na, file = paste0(dir_path, "dados/intermediate/contagens_automaticas_NA_dia_semana.csv"))
remove(contagens_na)

contagens_auto <- contagens_auto %>% 
  filter(!is.na(TOTAL))

# exluir zeros entre 6 e 21h ----
# verificando zeros
contagens_zero <- contagens_auto %>% filter(TOTAL == 0)
# table(contagens_zero$FAIXA_HORA)
# hist(contagens_zero$FAIXA_HORA)

contagens_auto <- contagens_auto %>% 
  filter(TOTAL > 0 | (TOTAL == 0 & FAIXA_HORA %in% c(0, 1, 2, 3, 4, 5, 22, 23)))

remove(contagens_zero)

# exclui feriados ----
# ANBIMA: https://www.anbima.com.br/feriados/feriados.asp 
# https://github.com/joaopbini/feriados-brasil e 
# https://terminaldeinformacao.com/2017/06/28/tabela-todos-os-feriados-brasileiros/
feriados_nac <- read_excel(paste0(dir_path, "dados/input/feriados/feriados_nacionais.xls"), 
                           col_types = c("date", "text", "text")) %>%
  select(-`Dia da Semana`) %>%
  rename(DATA = Data)

feriados_sp <- tibble("DATA" = c("2016-07-09", "2016-11-20", "2016-01-25",
                                 "2017-07-09", "2017-11-20", "2017-01-25",
                                 "2018-07-09", "2018-11-20", "2018-01-25",
                                 "2019-07-09", "2019-11-20", "2019-01-25",
                                 "2020-07-09", "2020-11-20", "2020-01-25",
                                 "2021-07-09", "2021-11-20", "2021-01-25"),
                      "Feriado" = rep(
                        c("Revolução constitucionalista", 
                          "Consciência negra", 
                          "Aniversário cidade SP"),
                        times = 6)) %>%
  mutate(DATA = as.Date(DATA))

feriados <- rbind(feriados_nac, feriados_sp) %>%                    # une feriados nacionais com feriados de SP
  mutate(FERIADO = 1) %>%
  select(-Feriado)

contagens_auto <- contagens_auto %>%
  left_join(feriados, by = "DATA") %>%
  mutate(FERIADO = ifelse(is.na(FERIADO), 0, FERIADO))

contagens_auto <- contagens_auto %>%                              
  filter(FERIADO == 0)

remove(feriados, feriados_sp, feriados_nac)

##################################################################
#### PLANILHA DE LOCALIZAÇÃO - GEORREFERENCIAMENTO DOS PONTOS ####
##################################################################
# abrir a planilha com as localizações, selecionar variáveis de interesse e transformar em sf
localizacao <- 
  read_excel(paste0(
    #dir_path, "dados/input/contagens/site_CET/mov_localizacao_contadores_SET_2021.xlsx")) %>% 
    dir_path, "dados/input/contagens/site_CET/mov_localizacao_contadores_04_2022.xlsx")) %>% 
  select(ID_PTOS, LAT, LONG, CICLOVIA) %>% 
  mutate(lat = LAT, long = LONG) %>% 
  st_as_sf(coords = c("long", "lat"), crs = 4326)   # transformar o banco de dados de localização num objeto georreferenciado

########################################
#### SALVAR ARQUIVOS INTERMEDIÁRIOS ####
########################################
saveRDS(contagens_auto, paste0(dir_path, "dados/intermediate/contagens_auto.RDS"))
saveRDS(localizacao, paste0(dir_path, "dados/intermediate/localizacao.RDS"))

####################################################################
#### MANTER ARQUIVOS DE INTERESSE PARA USO NOS PRÓXIMOS CÓDIGOS ####
####################################################################
# útil para a função "source"
rm(list=setdiff(ls(), c("contagens_auto", "localizacao", "dir_path", 
                        "%notin%", "filter", "rm_accent", "select", 
                        "source")))

gc()
