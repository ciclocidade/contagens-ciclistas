######################################################################################
#### OBJETIVO: Criar banco de dados com todas as possíveis variáveis de interesse ####
######################################################################################
# 1) unir à tabela de localização dos pontos de contagem informações sobre o local;
# 2) unir a localização aos pontos. 

#####################################################
#### PUXANDO O BANCO DE DADOS DO CÓDIGO ANTERIOR ####
#####################################################
# dados contagens automáticas
# source(paste0(dir_path, "contagiante_gitlab/codigos/contagens_automaticas.R"))

# localização contagens automáticas
# source(paste0(dir_path, "contagiante_gitlab/codigos/localizacao_cont_automaticas.R"))

# dados contagens manuais
# source(paste0(dir_path, "contagiante_gitlab/codigos/contagens_manuais.R"))

# dados MSVP
# source(paste0(dir_path, "contagiante_gitlab/codigos/msvp_2018_2019.R"))

#########################
#### UNINDO OS DADOS ####
#########################
automaticas_agreg <- localizacao %>% 
  select(-CICLOVIA) %>% 
  right_join(automaticas_agreg, by = "ID_PTOS") %>% 
  mutate(ID_PTOS = paste(ID_PTOS, "automáticas", sep = "-"))

contagens <- bind_rows(automaticas_agreg, manuais_agreg, msvp_agreg)

remove(automaticas_agreg, manuais_agreg, msvp_agreg, localizacao)

###############################################################
#### SELECIONANDO PONTOS ÚNICOS PARA UNIR OUTRAS VARIÁVEIS ####
###############################################################
pontos_unicos <- contagens %>% count(ID_PTOS) %>% 
  mutate(CONTAGEM = str_split(ID_PTOS, "-", simplify = TRUE)[ , 2])

#######################################
#### VARIAVEIS CONSTANTES NO TEMPO ####
#######################################
# Variáveis constantes no tempo por dependerem apenas da localização dos pontos
# para essas variáveis eu vou fazer o join primeiro na tabela de localização, e depois passar para a tabela de dados

#  1) Distrito >> GEOSAMPA ----
distrito <- read_sf(paste0(dir_path, "dados/input/shp_distritos/SIRGAS_SHP_distrito_polygon.shp")) %>% 
  st_set_crs(31983) %>% 
  st_transform(4326) %>% 
  select(ds_nome, ds_codigo) %>% 
  rename(DISTRITO = ds_nome,
         DISTRITO_CD = ds_codigo) %>%
  mutate(DISTRITO_CD = as.numeric(DISTRITO_CD))

pontos_unicos <- pontos_unicos %>% st_intersection(distrito)

remove(distrito)

#  2) Subprefeitura >> GEOSAMPA ----
sub <- read_sf(paste0(dir_path, "dados/input/shp_subprefeituras/SIRGAS_SHP_subprefeitura_polygon.shp")) %>% 
  st_set_crs(31983) %>%                                     # define o CRS padrão do GEOSAMP
  st_transform(4326) %>%                                    # transforma para WGS 84
  select(sp_nome, sp_codigo) %>%                            # seleciona colunas de interesse
  rename(SUB = sp_nome,
         SUB_CD = sp_codigo)                                # renomeia colunas de interesse

pontos_unicos <- pontos_unicos %>% st_intersection(sub)         # une as informações ao banco de pontos

remove(sub)

#  3) Região da cidade (região 8) >> GEOSAMPA ----
regiao8 <- read_sf(paste0(dir_path, "dados/input/shp_regiao8/SIRGAS_REGIAO8.shp")) %>% 
  st_set_crs(31983) %>%                                     # define o CRS padrão do GEOSAMP
  st_transform(4326) %>%                                    # transforma para WGS 84
  select(NOME) %>%                                          # seleciona colunas de interesse
  rename(REG8 = NOME)                                       # renomeia colunas de interesse

pontos_unicos <- pontos_unicos %>% st_intersection(regiao8)     # une as informações ao banco de pontos

remove(regiao8)

#  4) índice de desenvolvimento humano municipal (unidade=setor censitário) >> PMSP SMUL ----
# puxei esse dado direto da Secretaria de uRBANISMO E lICENSIAMENTO, já agregado por subprefeitura
# https://www.prefeitura.sp.gov.br/cidade/secretarias/licenciamento/desenvolvimento_urbano/dados_estatisticos/info_cidade/demografia/index.php?p=260265
# na época não havia sub Sapopemba, que era junto com Vila Prudente. Copiei os dados de VP para Sapopemba
idh <- read_excel(paste0(dir_path,"dados/input/7_Indice_de_desenvolvimento_humano_municip_2000_10962.xls")) %>%
  select(matches("_2010|SUB"))                              # seleciona colunas de interesse com base em um padrão

pontos_unicos <- pontos_unicos %>% left_join(idh, by = "SUB")   # une as informações ao banco de pontos

remove(idh)

#  5) classificação viária da via >> GEOSAMPA  ----
c_viaria <- read_sf(paste0(dir_path, "dados/input/shp_classeviariacet/SIRGAS_SHP_classeviariacet.shp")) %>% #abrir
  st_set_crs(31983) %>% st_transform(4326) %>%              # assinala e transforma CRS
  select(cvc_classe, cvc_codlog) %>%                        # seleciona variável de interesse
  mutate(ID_LOGR = row_number())                            # criar ID com o número da linha

c_viaria_df <- c_viaria %>%                                 # transforma objeto sf em df
  st_set_geometry(NULL) %>% 
  as_tibble() %>% 
  rename(CVC_CLASSE = cvc_classe,
         LG_CODLOG = cvc_codlog)                            # renomeia as variáveis de interesse

pontos_unicos <- pontos_unicos %>% 
  mutate(ID_LOGR = st_nearest_feature(
    pontos_unicos, c_viaria)) %>%                             # busca via mais próxima ao ponto
  left_join(c_viaria_df, by = "ID_LOGR") %>%                # une com a tabela de dados pelo ID
  select(-ID_LOGR)                                          # exclui variável criada apenas para o join

remove(c_viaria, c_viaria_df)

# 10) zona OD e densidade populacional (pop/km2) >> OD 2017 ----
shp_OD2017 <- read_sf(paste0(dir_path, "dados/input/shp_OD_2017/Zonas_2017_region.shp")) %>% 
  st_set_crs(22523) %>% st_transform(4326) %>%                    # assinala e transforma CRS    
  rename(ZONA_OD = NumeroZona)                                    # renomeia variável de interesse

OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_PESS, FE_PESS) %>%                              # seleciona variável de interesse
  rename(ZONA_OD = ZONA) %>%                                      # renomeia variável de interesse
  distinct(ID_PESS, .keep_all = TRUE) %>%                         # mantém uma observação por pessoa
  group_by(ZONA_OD) %>% 
  summarise(POP = sum(FE_PESS, na.rm = TRUE))                     # calcula a população total por ZONA_OD
#sum(OD2017$pop) #18.963.462

shp_OD2017 <- shp_OD2017 %>% 
  left_join(OD2017, by = "ZONA_OD") %>%                           # une o dado ao shp
  st_transform(31983) %>%                                         # passa o CRS para UTM (medido em metros)
  mutate(AREA = st_area(.),                                       # calcula a área de cada zona
         AREA_KM2 = AREA/1000000) %>% 
  st_transform(4326) %>%                                          # volta o CRS para WGS 84
  mutate(POP_KM2 = as.numeric(POP/AREA_KM2)) %>%                  # calcula população por km2
  select(ZONA_OD, POP_KM2)                                        # seleciona variáveis de interesse

pontos_unicos <- pontos_unicos %>% st_intersection(shp_OD2017)    # une as informações ao banco de pontos

remove(shp_OD2017, OD2017)

#  6) número de espaços verdes na ZONA_OD >> GEOSAMPA ----
shp_OD2017 <- read_sf(paste0(dir_path, "dados/input/shp_OD_2017/Zonas_2017_region.shp")) %>% 
  st_set_crs(22523) %>% st_transform(4326) %>%                    # assinala e transforma CRS    
  rename(ZONA_OD = NumeroZona)

parques <- read_sf(paste0(dir_path, "dados/input/shp_parquemunicipal/SIRGAS_SHP_parquemunicipal.shp")) %>% # abrir
  st_set_crs(31983) %>% st_transform(4326)                  # assinalar e transformar CRS

parques <- parques %>% st_intersection(shp_OD2017) %>% 
  distinct(ZONA_OD, pq_nome) %>% 
  count(ZONA_OD, name = "PARQUES_OD")

pontos_unicos <- pontos_unicos %>% 
  left_join(parques, by = "ZONA_OD") %>% 
  mutate(PARQUES_OD = ifelse(is.na(PARQUES_OD), 0, PARQUES_OD))

remove(parques)

# pontos_unicos <- pontos_unicos %>% 
#   mutate(D_PARQUE = apply(  
#     st_distance(pontos_unicos, parques),                      # calcula a distância até todos os parques
#     1, min))                                                  # escolhe a menor distância

#  7) número de empregos no distrito >> RAIS ----
rais_com_ser_2019 <- read_delim(paste0(dir_path, "dados/input/RAIS/CSV/RAIS_ESTAB_EMPR_COMERCIO_SERVICO_PORTE_2019.csv"), 
                                ";", escape_double = FALSE, 
                                locale = locale(decimal_mark = ",", grouping_mark = "."), 
                                trim_ws = TRUE) %>% 
  select(DISTRITO, COD_DIST, EMPR_MICRO, EMPR_PEQ, EMPR_MED, EMPR_GDE)

rais_ind_2019 <- read_delim(paste0(dir_path, "dados/input/RAIS/CSV/RAIS_ESTAB_EMPR_INDUSTRIA_PORTE_2019.csv"), 
                            ",", escape_double = FALSE, 
                            locale = locale(decimal_mark = ",", grouping_mark = "."), 
                            trim_ws = TRUE) %>%
  select(DISTRITO, COD_DIST, EMPR_MICRO, EMPR_PEQ, EMPR_MED, EMPR_GDE)

rais19 <- rbind(rais_com_ser_2019, rais_ind_2019)           # une base de comércio e serviços com indústria

rais19 <- rais19 %>%                                        # para cada distrito soma todos os postos de emprego
  filter(DISTRITO != "N.L") %>% 
  group_by(COD_DIST) %>% 
  summarise(EMPREGOS_2019 = sum(EMPR_MICRO + EMPR_PEQ + EMPR_MED + EMPR_GDE, na.rm = TRUE)) %>%
  rename(DISTRITO_CD = COD_DIST) %>%
  mutate(DISTRITO_CD = as.numeric(DISTRITO_CD))

pontos_unicos <- pontos_unicos %>% left_join(rais19, by = "DISTRITO_CD")  # une as informações ao banco de pontos

remove(rais19, rais_com_ser_2019, rais_ind_2019)

#  8) distância até instituições de ensinio >> GEOSAMPA ----
# como havia várias plabilhas para os equipamentos de ensino, uni todas em uma única
files_educ <- grep(list.files(path = paste0(dir_path, "dados/input/shp_equip_educacao_estudantes/"), 
                              all.files = FALSE,
                              full.names = TRUE), 
                   pattern=".cpg|.dbf|.shx", 
                   inv=TRUE, 
                   value=TRUE)

educ <- map_df(files_educ, read_sf) %>% 
  st_set_crs(31983) %>% st_transform(4326)

educ <- educ %>% st_intersection(shp_OD2017) %>% 
  st_set_geometry(NULL) %>% as_tibble() %>% 
  group_by(ZONA_OD) %>%
  summarise(EQ_ENSINO_OD = n(),
            ALUNOS_OD = sum(as.numeric(ALTOTAL), na.rm = TRUE))

pontos_unicos <- pontos_unicos %>% 
  left_join(educ, by = "ZONA_OD") %>% 
  mutate(EQ_ENSINO_OD = ifelse(is.na(EQ_ENSINO_OD), 0, EQ_ENSINO_OD),
         ALUNOS_OD = ifelse(is.na(ALUNOS_OD), 0, ALUNOS_OD))           

remove(educ, files_educ)

#  9) zoneamento >> GEOSAMPA -- PROBLEMA ----
# https://gestaourbana.prefeitura.sp.gov.br/marco-regulatorio/zoneamento/arquivos/
# como haviam vários arquivos de zoneamento, juntei todos em uma lista
files_zon <- grep(list.files(path = paste0(dir_path, "dados/input/shp_zoneamento/"), 
                              all.files = FALSE,
                              full.names = FALSE), 
                   pattern=".shp", 
                   inv=FALSE, 
                   value=TRUE)

# Desgna o tipo "MULTPOLYGON" para todos os shapes e 
#   faz a intersecção com os pontos para saber qual o zoneamento em cada ponto
list_int <- list()
for(i in seq_along(files_zon)){
  zon <- read_sf(paste0(dir_path, "dados/input/shp_zoneamento/", files_zon[i])) 
  
  if (files_zon[i] == "ZEIS_5.shp") {
    zon <- zon %>% filter(!is.na(ZONA) & a > 0)
  }
  
  if (is.na(st_crs(zon))) {
    zon <- zon %>% 
      st_set_crs(31983) %>% st_transform(4326)
  }else{
    zon <- zon %>% st_transform(4326)
  }
  
  zon <- st_cast(zon,"MULTIPOLYGON")
  
  print(i)
  
  list_int[[i]] <- zon
}

list_int <- bind_rows(list_int) 

shp_OD2017 <- read_sf(paste0(dir_path, "dados/input/shp_OD_2017/Zonas_2017_region.shp")) %>% 
  st_set_crs(22523) %>% st_transform(4326) %>%                    # assinala e transforma CRS    
  rename(ZONA_OD = NumeroZona) %>% 
  filter(NumeroMuni == 36) %>% 
  select(ZONA_OD)

#para evitar 'TopologyException: Input geom 1 is invalid' self-intersection error
zonas_od <- shp_OD2017$ZONA_OD
list_int <- list_int %>% st_buffer(dist = 0.000001)

shp_OD2017 <- shp_OD2017 %>% st_buffer(dist = 0.000001)

list_OD <- list()
for (i in seq_along(zonas_od)) {
  shp_OD2017_aux <- shp_OD2017 %>%  filter(ZONA_OD == zonas_od[i])
  
  shp_OD2017_aux <- list_int %>% st_intersection(shp_OD2017_aux) %>% 
    mutate(area = st_area(.)) %>% 
    group_by(ZONA) %>% 
    summarise(area = sum(area, na.rm = TRUE)) %>% 
    slice_max(., order_by = area) %>% 
    st_set_geometry(NULL) %>% as_tibble() %>% 
    mutate(ZONA_OD = zonas_od[i])
  
  list_OD[[i]] <- shp_OD2017_aux
  
  print(length(zonas_od) - i)
}

OD_zoneamento <- bind_rows(list_OD) %>% 
  select(-area)

pontos_unicos <- pontos_unicos %>% left_join(OD_zoneamento, by = "ZONA_OD")

remove(zon, list_int, files_zon, i, list_OD, OD_zoneamento,
       shp_OD2017_aux, shp_OD2017, zonas_od)


# 11) idade média/mediana >> OD 2017 ----
OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_PESS, FE_PESS, IDADE) %>%                       # seleciona variável de interesse       
  rename(ZONA_OD = ZONA) %>%                                      # renomeia variável de interesse  
  filter(FE_PESS > 0) %>%                                         # filtra para fator de expansão maior que zero
  distinct(ID_PESS, .keep_all = TRUE) %>%                         # mantém uma observação por pessoa
  group_by(ZONA_OD) %>%
  summarise(IDADE_MEDIA = weighted.mean(IDADE, w = FE_PESS, na.rm = TRUE),      # calcula a idade média
            IDADE_MEDIANA = weighted.median(IDADE, w = FE_PESS, na.rm = TRUE))  # calcula a idade mediana

pontos_unicos <- pontos_unicos %>% left_join(OD2017, by = "ZONA_OD")  # une as informações ao banco de pontos

remove(OD2017)

# 12) grau de instrução >> OD 2017 ----
OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_PESS, FE_PESS, GRAU_INS) %>%                     # seleciona variável de interesse  
  rename(ZONA_OD = ZONA) %>%                                       # renomeia variável de interesse  
  mutate(MEDIO_INCOMPLETO = ifelse(GRAU_INS < 4, 1*FE_PESS, 0),    # cria variáveis de interesse
         MEDIO_COMPLETO = ifelse(GRAU_INS %in% c(4, 5), 1*FE_PESS, 0),
         SUPERIOR_INCOMPLETO = ifelse(GRAU_INS < 5, 1*FE_PESS, 0),
         SUPERIOR_COMPLETO = ifelse(GRAU_INS == 5, 1*FE_PESS, 0)) %>% 
  distinct(ID_PESS, .keep_all = TRUE) %>%                          # deixa uma observação por pessoa
  group_by(ZONA_OD) %>%                                            # cria variáveis em nível a nìvel de zona
  summarise(POP = sum(FE_PESS, na.rm = TRUE),
            MEDIO_INC = sum(MEDIO_INCOMPLETO, na.rm = TRUE),
            MEDIO_COM = sum(MEDIO_COMPLETO, na.rm = TRUE),
            SUPERIOR_INC = sum(SUPERIOR_INCOMPLETO, na.rm = TRUE),
            SUPERIOR_COM = sum(SUPERIOR_COMPLETO, na.rm = TRUE)) %>% 
  mutate(MEDIO_INC =  MEDIO_INC/POP,                              # transforma variáveis para proporções
         MEDIO_COM =  MEDIO_COM/POP,
         SUPERIOR_INC = SUPERIOR_INC/POP,
         SUPERIOR_COM = SUPERIOR_COM/POP) %>% 
  select(-POP)
  
pontos_unicos <- pontos_unicos %>% left_join(OD2017, by = "ZONA_OD")  # une as informações ao banco de pontos

remove(OD2017)

# 13) proporção de população masculina >> OD 2017 ----
OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_PESS, FE_PESS, SEXO) %>%                        # seleciona variável de interesse   
  rename(ZONA_OD = ZONA) %>%                                      # renomeia variável de interesse 
  mutate(FEMININO = ifelse(SEXO == 2, 1*FE_PESS, 0)) %>%          # reclassifica variável sexo como dummy para fem
  distinct(ID_PESS, .keep_all = TRUE) %>%                         # deixa uma observação por pessoa
  group_by(ZONA_OD) %>%                                           # cria variáveis em nível a nìvel de zona
  summarise(POP = sum(FE_PESS, na.rm = TRUE),
            FEMININO = sum(FEMININO, na.rm = TRUE)) %>%    
  mutate(FEMININO =  FEMININO/POP) %>%                            # transforma variáveis para proporções
  select(-POP)

pontos_unicos <- pontos_unicos %>% left_join(OD2017, by = "ZONA_OD")  # une as informações ao banco de pontos

remove(OD2017)

# 14) proporção da população que vai ao trabalho de bicicleta >> OD 2017 ----
OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_PESS, FE_VIA, MODOPRIN, MOTIVO_D) %>%           # seleciona variável de interesse       
  rename(ZONA_OD = ZONA) %>%                                      # renomeia variável de interesse       
  filter(MOTIVO_D %in% c(1,2,3,9)) %>%                            # filtra para motivos de trabalho apenas    
  mutate(VIAGENS_BICI = ifelse(MODOPRIN == 15, 1*FE_VIA, 0)) %>%  # expande o total de viagens        
  distinct(ID_PESS, .keep_all = TRUE) %>%                         # deixa uma observação por pessoa
  group_by(ZONA_OD) %>%                                           # cria variáveis em nível a nìvel de zona
  summarise(VIAGENS = sum(FE_VIA, na.rm = TRUE),
            VIAGENS_BICI = sum(VIAGENS_BICI, na.rm = TRUE)) %>% 
  mutate(VIAGENS_BICI =  VIAGENS_BICI/VIAGENS) %>%                # transforma variáveis para proporções
  select(-VIAGENS)

pontos_unicos <- pontos_unicos %>% left_join(OD2017, by = "ZONA_OD")  # une as informações ao banco de pontos

remove(OD2017)

# 15) renda média/mediana dos docmicílios >> OD 2017 ----
OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_FAM, FE_FAM, RENDA_FA, NO_MORAF) %>%            # seleciona variável de interesse              
  rename(ZONA_OD = ZONA) %>%                                      # renomeia variável de interesse       
  filter(FE_FAM > 0) %>%                                          # filtra para famílias com fator de expansão
  distinct(ID_FAM, .keep_all = TRUE) %>%                          # deixa uma observação por família
  group_by(ZONA_OD) %>%                                           # cria variáveis ponderadas a nìvel de zona     
  summarise(RENDA_FAMILIAR_MEDIA = weighted.mean(RENDA_FA, w = FE_FAM, na.rm = TRUE),
            RENDA_FAMILIAR_MEDIANA = weighted.median(RENDA_FA, w = FE_FAM, na.rm = TRUE),
            PESS_FAMILIA = weighted.mean(NO_MORAF, w = FE_FAM, na.rm = TRUE))

pontos_unicos <- pontos_unicos %>% left_join(OD2017, by = "ZONA_OD")  # une as informações ao banco de pontos

remove(OD2017)
# 16) proporção de domicílios com posse de bicicleta, auto e motocicleta >> OD 2017 ----
OD2017 <- read.dbf(paste0(dir_path, "dados/input/shp_OD_2017/OD_2017.dbf")) %>% 
  select(ZONA, ID_DOM, FE_DOM, NO_MORAD, QT_AUTO, QT_MOTO, QT_BICICLE) %>%  # seleciona variável de interesse                
  rename(ZONA_OD = ZONA) %>%                                       # renomeia variável de interesse          
  filter(FE_DOM > 0) %>%                                           # filtra para domicílios com fator de expansão        
  distinct(ID_DOM, .keep_all = TRUE) %>%                           # deixa uma observação por domicílio
  mutate(P_AUTO = ifelse(QT_AUTO > 0, 1, 0),                       # cria dummies para posse
         P_MOTO = ifelse(QT_MOTO > 0, 1, 0),
         P_BICI = ifelse(QT_BICICLE > 0, 1, 0),
         AUTO_PESS = ifelse(NO_MORAD == 0, 0, QT_AUTO/NO_MORAD),
         MOTO_PESS = ifelse(NO_MORAD == 0, 0, QT_MOTO/NO_MORAD),
         BICI_PESS = ifelse(NO_MORAD == 0, 0, QT_BICICLE/NO_MORAD)) %>%
  group_by(ZONA_OD) %>%                                           # cria variáveis ponderadas a nìvel de zona
  summarise(DOMICILIOS = sum(FE_DOM, na.rm = TRUE),
            DOM_P_AUTO = sum(FE_DOM * P_AUTO, na.rm = TRUE),
            DOM_P_MOTO = sum(FE_DOM * P_MOTO, na.rm = TRUE),
            DOM_P_BICI = sum(FE_DOM * P_BICI, na.rm = TRUE),
            AUTO_PESSOA = weighted.mean(AUTO_PESS, w = FE_DOM, na.rm = TRUE),
            MOTO_PESSOA = weighted.mean(MOTO_PESS, w = FE_DOM, na.rm = TRUE),
            BICI_PESSOA = weighted.mean(BICI_PESS, w = FE_DOM, na.rm = TRUE)) %>%
  mutate(PROP_DOM_AUTO = DOM_P_AUTO/DOMICILIOS,
         PROP_DOM_MOTO = DOM_P_MOTO/DOMICILIOS,
         PROP_DOM_BICI = DOM_P_BICI/DOMICILIOS,
         TX_MOTORIZ = AUTO_PESSOA * 1000) %>%
  select(ZONA_OD, PROP_DOM_AUTO, PROP_DOM_MOTO, PROP_DOM_BICI, 
         TX_MOTORIZ, AUTO_PESSOA, MOTO_PESSOA, BICI_PESSOA) 

pontos_unicos <- pontos_unicos %>% left_join(OD2017, by = "ZONA_OD")  # une as informações ao banco de pontos

remove(OD2017)

# 17) inclinação da via >> FLAVIO/GEOSAMPA ----
# library(sp)
# library(maptools)
# library(rgdal)
# library(raster)
# library(rgeos)
regiao8 <- read_sf(paste0(dir_path, "dados/input/shp_regiao8/SIRGAS_REGIAO8.shp")) %>% 
  st_set_crs(31983) %>% 
  st_transform(4326)

RedeViaria <-  st_read(paste0(dir_path, "dados/input/shp_logradouros/SIRGAS_SHP_logradouronbl.shp")) %>% #abrir rede viária
  st_set_crs(31983) %>% st_transform(4326)

pontos_buffer <- pontos_unicos %>% 
  st_transform(31983) %>% 
  st_buffer(100) %>% 
  st_transform(4326)

RedeViaria <- RedeViaria %>% st_intersection(pontos_buffer) %>% st_cast("LINESTRING")

pontos <- st_read(paste0(dir_path, "dados/input/elevacao/geosampa_pontos_de_elevacao_no_viario.gpkg"))

ext <- extent(as_Spatial(regiao8))

contourRaster <- raster(ext = ext,
                        crs = 4326) # está gerando pixels de 100 x 300 metros 

p <- as_Spatial(pontos)

result <- rasterize(p, contourRaster, field = "ISOVALOR")

# a melhor interpolação seria com IDW, mas demora mtooooo
# library(gstat)
# g <- gstat(id="ISOVALOR", formula = ISOVALOR~1, data=p,
#            nmax=7, set=list(idp = .5))
# interpDEM <- interpolate(contourRaster, g)
# 
# plot(interpDEM)

RedeViaria$DECLIV = slope_raster(RedeViaria, dem = result)    # calculando a declividade de forma absoluta 

RedeViaria <- RedeViaria %>%   
  mutate(DECLIV = ifelse(is.na(DECLIV),                       # dois pontos de contagem estavam ficando com NA, 
                         mean(DECLIV, na.rm = TRUE), DECLIV)) #vamos passar a declividade média de SP para eles.

RedeViaria <- RedeViaria %>% mutate(DECLIV = DECLIV * 100) # passando a declividade para porcentagem

RedeViaria <- RedeViaria %>%                               # criando classes para a declividade
  mutate(DECLIV_CLASS = case_when(DECLIV <= 3 ~ "0-3: plano",
                                  DECLIV >  3 & DECLIV <= 5 ~ "3-5: leve",
                                  DECLIV >  5 & DECLIV <= 8 ~ "5-8: médio",
                                  DECLIV >  8 & DECLIV <= 10 ~ "8-10: exigente",
                                  DECLIV > 10 & DECLIV <= 20 ~ "10-20: terrível",
                                  DECLIV > 20 ~ "20+: impossível"))

RedeViaria <- RedeViaria %>%                    
  select(DECLIV, DECLIV_CLASS, lg_codlog, lg_nome) %>%    # selecionar variável de interesse
  mutate(ID_LOGR = row_number()) %>%                                # criar ID com o número da linha
  rename(CODLOG = lg_codlog,
         NOME_LOG = lg_nome)

RedeViaria_df <- RedeViaria %>%                                     # transformar objeto sf em df
  st_set_geometry(NULL) %>% 
  as_tibble() 

pontos_unicos <- pontos_unicos %>% 
  mutate(ID_LOGR = st_nearest_feature(pontos_unicos, RedeViaria)) %>% # buscar via mais próxima ao ponto
  left_join(RedeViaria_df, by = "ID_LOGR") %>%                      # unir com a tabela de dados pelo ID
  select(-ID_LOGR)

remove(contourRaster, ext, p, pontos, pontos_buffer, RedeViaria, RedeViaria_df, regiao8, result)

# 18) raça predominante (ou transformar para % de brancos? - SIM) >> CENSO 2010 ----
# abrindo arquivos do censo
Pessoa03_SP1 <- read_delim(paste0(dir_path,"dados/input/Censo/CSV/Pessoa03_SP1.csv"), 
                           ";", escape_double = FALSE, 
                           col_types = cols(Cod_setor = col_character(),
                                            V001 = col_number(), 
                                            V002 = col_number()), 
                           trim_ws = TRUE) %>%
  select(Cod_setor, V001, V002) %>%        # seleciona variáveis de interesse
  rename(CD_GEOCODI = Cod_setor,                                   # renomeia variáveis de interesse
         RESIDENTES = V001,
         BRANCOS = V002) 

de_para_OD_censo <- read_delim(
  paste0(dir_path,"dados/input/shp_setores_censitarios/Censo2010_Relacao_CodSetor_ZonasOD.csv"), 
                               ";", escape_double = FALSE, 
                               col_types = cols(CD_GEOCODI = col_character(),
                                                ZONA_OD = col_number()), 
                           trim_ws = TRUE)

Pessoa03_SP1 <- Pessoa03_SP1 %>% 
  left_join(de_para_OD_censo, by = "CD_GEOCODI") %>%
  group_by(ZONA_OD) %>% 
  summarise(BRANCOS = sum(BRANCOS, na.rm = TRUE),
            RESIDENTES = sum(RESIDENTES, na.rm = TRUE)) %>% 
  mutate(PCT_BRANCOS = ifelse(RESIDENTES == 0, 0, BRANCOS/RESIDENTES)) %>%  # cria proporção de brancos por zona OD
  select(ZONA_OD, PCT_BRANCOS)

pontos_unicos <- pontos_unicos %>% left_join(Pessoa03_SP1, by = "ZONA_OD") # une as informações ao banco de pontos

remove(Pessoa03_SP1, de_para_OD_censo)

# 19) infraestrutura cicloviária (tipo) >> já vem na PLANILHA DE LOCALIZAÇÃO  ----
source(paste0(dir_path, "contagiante_gitlab/codigos/localizacao_cont_automaticas.R"))
localizacao <- localizacao %>% 
  mutate(ID_PTOS = paste(ID_PTOS, "automáticas", sep = "-")) %>% 
  st_set_geometry(NULL) %>% as.data.frame()

pontos_unicos <- pontos_unicos %>% 
  left_join(localizacao, by = "ID_PTOS") %>% 
  mutate(TP_CICLO = case_when(str_detect(CICLOVIA, "CICLOVIA") ~ "CICLOVIA",
                              str_detect(CICLOVIA, "CICLOFAIXA") ~ "CICLOFAIXA",
                              str_detect(CICLOVIA, "PTE") ~ "PONTE",
                              TRUE ~ "NAO_IDENT")) %>%
  select(-CICLOVIA)

remove(localizacao)

# 20) distância para estação de transporte de massa (trem, metrô, monotrilho e terminal de ônibus) >> GEOSAMPA ----
# como não tem a data de inauguração das estações, vai ficar como informação fixa (queríamos que fosse anual)
shp_OD2017 <- read_sf(paste0(dir_path, "dados/input/shp_OD_2017/Zonas_2017_region.shp")) %>% 
  st_set_crs(22523) %>% st_transform(4326) %>%                    # assinala e transforma CRS    
  rename(ZONA_OD = NumeroZona)

trens <- read_sf(paste0(dir_path, "dados/input/shp_estacaotrem/SIRGAS_SHP_estacaotrem_point.shp")) %>%
  st_set_crs(31983) %>% st_transform(4326) %>%                    # assinalar e transformar CRS
  mutate(TIPO = "trem") %>%
  select(TIPO)

metro <- read_sf(paste0(dir_path, "dados/input/shp_estacaometro/SIRGAS_SHP_estacaometro_point.shp")) %>%
  st_set_crs(31983) %>% st_transform(4326) %>%                    # assinalar e transformar CRS
  mutate(TIPO = "metro") %>%
  select(TIPO)

terminais <- read_sf(paste0(dir_path, "dados/input/shp_terminais_onibus/sirgas_terminal_onibus.shp")) %>%
  st_set_crs(31983) %>% st_transform(4326) %>%                    # assinalar e transformar CRS
  mutate(TIPO = "onibus") %>%
  select(TIPO)

transp_massa <- rbind(trens, metro, terminais)                    # une todos os tipos em um único arquivo

transp_massa <- transp_massa %>% st_intersection(shp_OD2017) %>% 
  st_set_geometry(NULL) %>% as_tibble() %>% 
  count(ZONA_OD, name = "TRANSPORTE_OD") 

pontos_unicos <- pontos_unicos %>% 
  left_join(transp_massa, by = "ZONA_OD") %>% 
  mutate(TRANSPORTE_OD = ifelse(is.na(TRANSPORTE_OD), 0, TRANSPORTE_OD))           

remove(transp_massa, trens, metro, terminais)

# 21) UPS (unidade padrão de severidade) >> ANUAL >> FLAVIO ----
ups <- st_read(paste0(dir_path, "dados/input/shp_UPS/SIRGAS_Geosampa_Logradouros_20210711_com_UPS.gpkg"), 
               layer='SIRGAS_Geosampa_Logradouros_20210711_com_UPS') %>% 
  st_set_crs(31983) %>% 
  st_transform(4326) %>% 
  select(lg_codlog, ups_km, ups_class) %>% 
  rename(LG_CODLOG = lg_codlog,
         UPS_KM = ups_km,
         UPS_CLASS = ups_class)%>%                                # transformar objeto sf em df
  st_set_geometry(NULL) %>% 
  as_tibble() %>% 
  distinct(LG_CODLOG, .keep_all = TRUE)                           # mantém uma obs por código de logradouro    

pontos_unicos <- pontos_unicos %>% left_join(ups, by = "LG_CODLOG")

remove(ups)

###############################################################
#### UNINDO OS DADOS FIXOS DOS PONTOS AO BANCO DE CONTAGEM ####
###############################################################
contagens <- contagens %>%                                
  st_set_geometry(NULL) %>% 
  as_tibble() %>% left_join(pontos_unicos, by = "ID_PTOS")

###########################################################################
#### INCLUINDO NO BANCO DE CONTAGEM AS VARIÁVEIS QUE MUDAM COM O TEMPO ####
###########################################################################
# quais variáveis variam com o tempo?
# DADOS ANUAIS ----
#  1) distância até bicicletários >> ANUAL >> FLAVIO ----
shp_OD2017 <- read_sf(paste0(dir_path, "dados/input/shp_OD_2017/Zonas_2017_region.shp")) %>% 
  st_set_crs(22523) %>% st_transform(4326) %>%                    # assinala e transforma CRS    
  rename(ZONA_OD = NumeroZona) %>% 
  select(ZONA_OD)

bicicletarios_2021 <- read_delim(
  paste0(dir_path, "dados/input/shp_bicicletarioparaciclo/bicicletarios_sp_2021.csv"), 
                                    ";", escape_double = FALSE, trim_ws = TRUE)

# ano inauguração
anos_inauguracao <- c("2021", "2020", "2019", "2018", "2017", "2016")

# assigna o bicicletário mais próximo à contagem ano a ano
list_contagens <- list()
for (i in seq_along(anos_inauguracao)) {
  # filtra todas os pontos com contagens de um ano específico
  pontos_aux <- pontos_unicos %>% select(ID_PTOS) %>%  
    left_join(contagens, by = "ID_PTOS") %>% 
    filter(str_sub(as.character(DATA), start = 1, end = 4) == anos_inauguracao[i]) %>% 
    count(ID_PTOS) %>% 
    select(-n)
  
  # filtra os bicicletários do mesmo ano específico
  bicicletarios_aux <- bicicletarios_2021 %>% 
    filter(ANO_INAUGURAÇÃO <= as.numeric(anos_inauguracao[i])) %>% 
    st_as_sf(coords = c("LONGITUDE", "LATITUDE"), crs = 4326)
  
  pontos_aux <- pontos_aux %>% st_intersection(shp_OD2017) %>% 
    st_set_geometry(NULL) %>% as_tibble() %>% 
    count(ZONA_OD, name = "BICICLETARIOS_OD") 
  
  contagens_aux <- contagens %>%  
    filter(str_sub(as.character(DATA), start = 1, end = 4) == anos_inauguracao[i])
  
  list_contagens[[i]] <- contagens_aux %>% 
    left_join(pontos_aux, by = "ZONA_OD")
}

contagens <- bind_rows(list_contagens) 

remove(bicicletarios_2021, anos_inauguracao, i, pontos_aux,
       bicicletarios_aux, contagens_aux, list_contagens, shp_OD2017)

# DADOS DIÁRIOS ----
#  2) volume de tráfego na via >> DIÁRIO >> API CET (não incluímos)
#  3) temperatura >> DIÁRIO >> CGESP ----
anos <- c("2021", "2020", "2019", "2018", "2017", "2016")

dados_temp_min <- list()
dados_temp_max <- list()
for (i in seq_along(anos)) {
  dados_temp_min[[i]] <- read_excel(paste0(dir_path, "dados/input/temperatura/planilha_geral_temperatura_MIN.xlsx"), 
                                sheet = anos[i]) %>% 
    mutate(ST = ifelse(is.na(ST), NA_integer_, as.numeric(ST)),
           INMET = ifelse(is.na(INMET), NA_integer_, as.numeric(INMET)))
  
  dados_temp_max[[i]] <- read_excel(paste0(dir_path, "dados/input/temperatura/planilha_geral_temperatura_MAX.xlsx"), 
                                    sheet = anos[i]) %>% 
    mutate(INMET = ifelse(is.na(INMET), NA_integer_, as.numeric(INMET)))
}

dados_temp_min <- bind_rows(dados_temp_min) %>% 
  select(-aux, -a, -MARC) %>% 
  mutate(SB = VP,
         GU = IT,
         EM = PE,
         SE = case_when(is.na(ANH) ~ CGE,
                        is.na(CGE) ~ ANH,
                        TRUE ~ (ANH + CGE)/2),
         CT = case_when(is.na(SM) ~ IQ,
                        is.na(IQ) ~ SM,
                        TRUE ~ (SM + IQ)/2)) %>% 
  rename(CV = INMET) %>% 
  select(-CGE, -ANH) %>% 
  pivot_longer(-Dia, names_to = "local", values_to = "TEMP_MIN") 

dados_temp_max <- bind_rows(dados_temp_max) %>% 
  select(-aux, -a, -MARC) %>% 
  mutate(SB = VP,
         GU = IT,
         EM = PE,
         SE = case_when(is.na(ANH) ~ CGE,
                        is.na(CGE) ~ ANH,
                        TRUE ~ (ANH + CGE)/2),
         CT = case_when(is.na(SM) ~ IQ,
                        is.na(IQ) ~ SM,
                        TRUE ~ (SM + IQ)/2)) %>% 
  rename(CV = INMET) %>% 
  select(-CGE, -ANH) %>% 
  pivot_longer(-Dia, names_to = "local", values_to = "TEMP_MAX")

tabela_subs <- tibble("local" = c('PR', 'PJ', 'FO', 'CV', 'ST', 'JT', 'MG', 'LA', 'SE', 'BT', 'PI', 'VM', 'IP', 'SA', 'JA', 'AD', 'CL', 'MB', 'CS', 'PA', 'PE', 'EM', 'MP', 'IT', 'MO', 'AF', 'IQ', 'GU', 'VP', 'SM', 'CT', 'SB'),
                      "SUB_CD" = c('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32'))

dados_temp_min <- dados_temp_min %>% 
  left_join(tabela_subs, by = "local") %>% 
  select(-local) %>% 
  rename(DATA = Dia)

dados_temp_max <- dados_temp_max %>% 
  left_join(tabela_subs, by = "local") %>% 
  select(-local) %>% 
  rename(DATA = Dia)

contagens <- contagens %>% left_join(dados_temp_min, by = c("SUB_CD", "DATA"))
contagens <- contagens %>% left_join(dados_temp_max, by = c("SUB_CD", "DATA"))

remove(dados_temp_min, dados_temp_max, tabela_subs, anos, i)

#  4) preciptação >> DIÁRIO >> CGESP ----
# ler arquivos
anos <- c("2021", "2020", "2019", "2018", "2017", "2016")

dados_pluv <- list()
for (i in seq_along(anos)) {
  dados_pluv[[i]] <- read_excel(paste0(dir_path, "dados/input/pluviosidade/planilha_geral_pluviosidade.xlsx"), 
                                sheet = anos[i])
}

dados_pluv <- bind_rows(dados_pluv) %>% 
  select(-aux, -a, -`SE - Sé`, -`Consolação (CGE)`) %>% 
  pivot_longer(-Dia, names_to = "local", values_to = "PLUVIOSIDADE") %>%
  mutate(CHUVA = ifelse(PLUVIOSIDADE > 0, 1, 0))

tabela_subs <- tibble("local" = c('AD - Cidade Ademar', 'AF - Aricanduva / Vl. Formosa', 'BT - Butantã', 'CL - Campo Limpo', 'CS - Capela do Socorro', 'CT - Cidade Tiradentes', 'CV - Casa Verde', 'EM - Ermelino Matarazzo', 'FO - Freguesia do Ó', 'GU - Guaianazes', 'IP - Ipiranga', 'IQ - Itaquera', 'IT - Itaim Paulista', 'JA - Jabaquara', 'JT - Jaçanã / Tremembé', 'LA - Lapa', 'MB - M Boi Mirim', 'MG - Vl. Maria / Guilherme', 'MO - Móoca', 'MP - São Miguel Paulista', 'PA - Parelheiros', 'PE - Penha', 'PI - Pinheiros', 'PJ - Pirituba / Jaraguá', 'PR - Perus', 'SA - Santo Amaro', 'SB - Sapopemba', 'SE - media', 'SM - São Mateus', 'ST - Santana', 'VM - Vila Mariana', 'VP - Vila Prudente'),
                      "SUB" = c('CIDADE ADEMAR', 'ARICANDUVA-FORMOSA-CARRAO', 'BUTANTA', 'CAMPO LIMPO', 'CAPELA DO SOCORRO', 'CIDADE TIRADENTES', 'CASA VERDE-CACHOEIRINHA', 'ERMELINO MATARAZZO', 'FREGUESIA-BRASILANDIA', 'GUAIANASES', 'IPIRANGA', 'ITAQUERA', 'ITAIM PAULISTA', 'JABAQUARA', 'JACANA-TREMEMBE', 'LAPA', "M'BOI MIRIM", 'VILA MARIA-VILA GUILHERME', 'MOOCA', 'SAO MIGUEL', 'PARELHEIROS', 'PENHA', 'PINHEIROS', 'PIRITUBA-JARAGUA', 'PERUS', 'SANTO AMARO', 'SAPOPEMBA', 'SE', 'SAO MATEUS', 'SANTANA-TUCURUVI', 'VILA MARIANA', 'VILA PRUDENTE'),
                      "SUB_CD" = c('16', '26', '10', '17', '19', '31', '04', '22', '03', '28', '13', '27', '24', '15', '06', '08', '18', '07', '25', '23', '20', '21', '11', '02', '01', '14', '32', '09', '30', '05', '12', '29'))

dados_pluv <- dados_pluv %>% left_join(tabela_subs, by = "local") %>% 
  select(-local, -SUB) %>% 
  rename(DATA = Dia)

contagens <- contagens %>% left_join(dados_pluv, by = c("SUB_CD", "DATA"))

#graphics::plot(dados_pluv$Dia, dados_pluv$PLUVIOSIDADE)
#graphics::plot(contagens$DATA, contagens$PLUVIOSIDADE)

remove(dados_pluv, i, anos, tabela_subs)

#  5) dia da semana ----
# já está no banco de dados

#  6) feriados ----
# deverão ser excluídos os dias de feriado da análise.
feriados_nac <- read_excel(paste0(dir_path, "dados/input/feriados/feriados_nacionais.xls"), 
                           col_types = c("date", "text", "text")) %>%
  select(-`Dia da Semana`) %>%
  rename(DATA = Data)

feriados_sp <- tibble("DATA" = c("2016-07-09", "2016-11-20", "2016-01-25",
                                 "2017-07-09", "2017-11-20", "2017-01-25",
                                 "2018-07-09", "2018-11-20", "2018-01-25",
                                 "2019-07-09", "2019-11-20", "2019-01-25",
                                 "2020-07-09", "2020-11-20", "2020-01-25",
                                 "2021-07-09", "2021-11-20", "2021-01-25"),
                      "Feriado" = rep(
                        c("Revoluçãoo constitucionalista", 
                          "Consciência negra", 
                          "Aniversário cidade SP"),
                        times = 6)) %>%
  mutate(DATA = as.Date(DATA))

feriados <- rbind(feriados_nac, feriados_sp) %>%                    # une feriados nacionais com feriados de SP
  mutate(FERIADO = 1) %>%
  select(-Feriado)

contagens <- contagens %>%
  left_join(feriados, by = "DATA") %>%
  mutate(FERIADO = ifelse(is.na(FERIADO), 0, FERIADO))

remove(feriados, feriados_sp, feriados_nac)

#  7) ponte de feriado (não incluímos) ----

#  8) mês do ano ----
contagens <- contagens %>%
  mutate(MES = months(DATA))

#  9) ano ----
contagens <- contagens %>%
  mutate(ANO = str_sub(as.character(DATA), start = 1, end = 4))

# 10) DUMMY FARIA LIMA
contagens <- contagens %>%
  mutate(DUMMY_FARIA_LIMA = ifelse(ID_PTOS == "34-automáticas", "1", "0"))

# 11) DUMMY PANDEMIA
contagens <- contagens %>%
  mutate(DUMMY_PANDEMIA = ifelse(DATA >= "2020-04-01", "1", "0"))

##################################################################
#### SALVANDO O BANCO DE DADOS FINAL COM TODAS AS INFORMAÇÕES ####
##################################################################
saveRDS(contagens, paste0(dir_path, "dados/intermediate/contagens_full.RDS"))
saveRDS(pontos_unicos, paste0(dir_path, "dados/intermediate/pontos_unicos_full.RDS"))

gc()
