###############################################
#### REGRESSÕES FAIXAS-HORÁRIA >> DIA TODO ####
###############################################

# definindo a pasta para o projeto
# dir_path <- "C:/Users/taina/Dropbox/Trabalho/2021_ciclocidade_strava/"

##########################################################
#### ABRINDO BASES DE DADOS NECESSÁRIAS PARA O CÓDIGO ####
##########################################################
# abrindo base de dados (são provenientes do código "2_criar_db_localizacao.R")
# se você estiver fora do arquivo "1_main" vai precisar carregar as bases, com as linhas abaixo.
# source <- function(f, encoding = 'UTF-8') {
#   l <- readLines(f, encoding=encoding)
#   eval(parse(text=l),envir=.GlobalEnv)
# }
# source(paste0(dir_path, "contagiante_gitlab/codigos/2_criar_db_localizacao.R"))

##########################################################
#### LIMPEZA E TRANSFORMAÇÃO INICIAL DA BASE DE DADOS ####
##########################################################
# excluir feriados ----
contagens <- contagens %>%
  filter(FERIADO == 0)

# adicionando variavel de proporção para horas de pico ----
dados_regressao <- contagens %>%                                
  mutate(PCT_FAIXAS_HORARIAS = ifelse(TOTAL_DIA == 0, 0, TOTAL_PICO/TOTAL_DIA))      # define proporção de viagens que são feitas no pico

# criando dummies para períodos específicos ----
dados_regressao <- dados_regressao %>% 
  mutate(DUMMY_YELLOW = ifelse(DATA >= as.Date("2018-08-01") & 
                                 DATA <= as.Date("2020-03-31") & 
                                 ID_PTOS == "34-automáticas", 1, 0),
         DUMMY_PANDEMIA = ifelse(DATA >= as.Date("2020-04-01"), 1, 0),
         DUMMY_FARIA_LIMA = ifelse(ID_PTOS == "34-automáticas", 1, 0))

# definindo as variáveis do modelo de regressão ----
var_dep <- "TOTAL_DIA"                           # variavel dependente: a que queremos explicar

vari_indep <- "TOTAL_PICO"                       # variávei independete: nossa principal variável explicativa

dummies <- c("DUMMY_YELLOW", "DUMMY_PANDEMIA", "DUMMY_FARIA_LIMA") # dummies para períodos importantes

socio <- c("IDHM_2010", "POP_KM2", "IDADE_MEDIA", "FEMININO", "SUPERIOR_COM",
           "RENDA_FAMILIAR_MEDIANA", "PESS_FAMILIA", "PCT_BRANCOS")

               #locacionais
controles <- c("PARQUES_OD", "EQ_ENSINO_OD", "BICICLETARIOS_OD", "EMPREGOS_2019", "DECLIV", 
               #socioeconomicos
               "IDHM_2010", "POP_KM2", "IDADE_MEDIA", "FEMININO", "SUPERIOR_COM",
               "RENDA_FAMILIAR_MEDIANA", "PESS_FAMILIA", "PCT_BRANCOS", 
               #transporte
               "VIAGENS_BICI", # "PROP_DOM_AUTO", "PROP_DOM_MOTO", "PROP_DOM_BICI", 
               "AUTO_PESSOA", "MOTO_PESSOA", "BICI_PESSOA", 
               "UPS_CLASS",
               #outros
               "REG8",  
               "CVC_CLASSE", "TP_CICLO") 

# limpando o banco para o que de fato vamos usar
dados_regressao <- dados_regressao %>% 
  select(all_of(c("ID_PTOS", "DATA", "CONTAGEM", "PCT_FAIXAS_HORARIAS",
                  var_dep, vari_indep, dummies, controles))) %>% 
  na.omit()

##################################
#### HISTOGRAMA E DESCRITIVAS ####
##################################
dados_regressao %>% filter(CONTAGEM == "automáticas" & PCT_FAIXAS_HORARIAS >= 0.2) %>% 
  ggplot(aes( x = PCT_FAIXAS_HORARIAS)) +
  geom_histogram(color="black", fill = "#cccccc", size = 0.2)+
  ylab("Número de observações") +
  xlab("Participação das viagens das faixas horárias selecionadas sobre o total")+
  theme_classic() +
  theme(legend.position = "bottom")

ggsave(plot = last_plot(),
       filename = paste0(dir_path, "figuras/intermediate/histograma_pct_faixas_horarias.png"),
       width = 25,
       height = 15,
       units = "cm")

df_autos <- dados_regressao %>% filter(CONTAGEM == "automáticas")
df <- descritivas_numericas(df.input = (df_autos %>% select(PCT_FAIXAS_HORARIAS))) %>% 
  rbind(descritivas_numericas(df.input = (df_autos %>% 
                                            filter(ID_PTOS %in% c("4-automáticas", 
                                                                  "34-automáticas",
                                                                  "35-automáticas")) %>% 
                                            select(PCT_FAIXAS_HORARIAS) %>%
                                            rename(FIXOS = PCT_FAIXAS_HORARIAS)))) %>% 
  rbind(descritivas_numericas(df.input = (df_autos %>% 
                                            filter(ID_PTOS %notin% c("4-automáticas", 
                                                                  "34-automáticas",
                                                                  "35-automáticas")) %>% 
                                            select(PCT_FAIXAS_HORARIAS) %>%
                                            rename(MOVEIS = PCT_FAIXAS_HORARIAS))))
write.csv(df, 
          file = paste0(dir_path, "dados/output/descritivas_pcts.csv"))
remove(df, df_autos)

dados_regressao %>% 
  filter(CONTAGEM == "automáticas" & PCT_FAIXAS_HORARIAS >= 0.2) %>% 
  mutate(`Tipo contador` = ifelse(ID_PTOS %in% c("4-automáticas", 
                                             "34-automáticas",
                                             "35-automáticas"), "Fixos", "Móveis")) %>% 
  ggplot(aes( x = PCT_FAIXAS_HORARIAS)) +
  geom_histogram(color="black", fill = "#cccccc", size = 0.2)+
  facet_wrap(~`Tipo contador`)+
  ylab("Número de observações") +
  xlab("Participação das viagens das faixas horárias selecionadas sobre o total")+
  theme_classic() +
  theme(legend.position = "bottom")

ggsave(plot = last_plot(),
       filename = 
         paste0(dir_path, 
                "figuras/intermediate/histograma_pct_faixas_horarias_por_tipo_contador.png"),
       width = 30,
       height = 15,
       units = "cm")

##################################################
#### CRIANDO CSVs COM INFORMAÇÕES DESCRITIVAS ####
##################################################
# descritivas básicas para ajudar na apresentação ----
table(dados_regressao$DUMMY_YELLOW)
table(dados_regressao$DUMMY_PANDEMIA)
table(dados_regressao$DUMMY_FARIA_LIMA)

# média por ponto - GERAL ----
medias_por_ponto <- dados_regressao %>% 
  group_by(ID_PTOS) %>% 
  summarise(TOTAL_mean = mean(TOTAL_DIA, na.rm = TRUE),
            TOTAL_PICO_mean = mean(TOTAL_PICO, na.rm = TRUE),
            TOTAL_sd = sd(TOTAL_DIA, na.rm = TRUE),
            TOTAL_PICO_sd = sd(TOTAL_PICO, na.rm = TRUE))

write.csv(medias_por_ponto, 
          file = paste0(dir_path, "dados/output/medias_por_ponto.csv"))

# média por ponto - SÓ DUMMIES ----
medias_por_ponto_dummies <- dados_regressao %>% 
  select(ID_PTOS, TOTAL_DIA, TOTAL_PICO, DUMMY_YELLOW, DUMMY_PANDEMIA, DUMMY_FARIA_LIMA) %>% 
  pivot_longer(cols = c("DUMMY_YELLOW", "DUMMY_PANDEMIA", "DUMMY_FARIA_LIMA"), 
               names_to = "DUMMY", values_to = "VALORES") %>% 
  mutate(TOTAL_DIA_DUMMY = TOTAL_DIA * VALORES,
         TOTAL_PICO_DUMMY = TOTAL_PICO * VALORES) %>% 
  group_by(ID_PTOS, DUMMY) %>% 
  summarise(TOTAL_mean = mean(TOTAL_DIA_DUMMY, na.rm = TRUE),
            TOTAL_PICO_mean = mean(TOTAL_PICO_DUMMY, na.rm = TRUE),
            TOTAL_sd = sd(TOTAL_DIA_DUMMY, na.rm = TRUE),
            TOTAL_PICO_sd = sd(TOTAL_PICO_DUMMY, na.rm = TRUE)) %>% 
  filter(TOTAL_mean > 0)

write.csv(medias_por_ponto_dummies, 
          file = paste0(dir_path, "dados/output/medias_por_ponto_dummies.csv"))

# média para dummies ----
medias_dummies <- tibble("DUMMY" = c("DUMMY_YELLOW", "DUMMY_PANDEMIA", "DUMMY_FARIA_LIMA", "FARIA LIMA PRÉ YELLOW"),
                         "TOTA_DIA" = c(mean(dados_regressao$TOTAL_DIA[dados_regressao$DUMMY_YELLOW == 1]),
                                        mean(dados_regressao$TOTAL_DIA[dados_regressao$DUMMY_PANDEMIA == 1]),
                                        mean(dados_regressao$TOTAL_DIA[dados_regressao$DUMMY_FARIA_LIMA == 1]),
                                        mean(dados_regressao$TOTAL_DIA[dados_regressao$ID_PTOS == 34 & 
                                                                         dados_regressao$DATA < "2018-08-01"])),
                         "TOTA_DIA_sd" = c(sd(dados_regressao$TOTAL_DIA[dados_regressao$DUMMY_YELLOW == 1]),
                                           sd(dados_regressao$TOTAL_DIA[dados_regressao$DUMMY_PANDEMIA == 1]),
                                           sd(dados_regressao$TOTAL_DIA[dados_regressao$DUMMY_FARIA_LIMA == 1]),
                                           sd(dados_regressao$TOTAL_DIA[dados_regressao$ID_PTOS == 34 & 
                                                                            dados_regressao$DATA < "2018-08-01"])),
                         "TOTA_PICO" = c(mean(dados_regressao$TOTAL_PICO[dados_regressao$DUMMY_YELLOW == 1]),
                                         mean(dados_regressao$TOTAL_PICO[dados_regressao$DUMMY_PANDEMIA == 1]),
                                         mean(dados_regressao$TOTAL_PICO[dados_regressao$DUMMY_FARIA_LIMA == 1]),
                                         mean(dados_regressao$TOTAL_PICO[dados_regressao$ID_PTOS == 34 & 
                                                                          dados_regressao$DATA < "2018-08-01"])),
                         "TOTA_PICO_sd" = c(sd(dados_regressao$TOTAL_PICO[dados_regressao$DUMMY_YELLOW == 1]),
                                            sd(dados_regressao$TOTAL_PICO[dados_regressao$DUMMY_PANDEMIA == 1]),
                                            sd(dados_regressao$TOTAL_PICO[dados_regressao$DUMMY_FARIA_LIMA == 1]),
                                            sd(dados_regressao$TOTAL_PICO[dados_regressao$ID_PTOS == 34 & 
                                                                             dados_regressao$DATA < "2018-08-01"])))

write.csv(medias_dummies, 
          file = paste0(dir_path, "dados/output/medias_dummies.csv"))

# médias por região ´----
oeste <- tibble("REG8" = "Oeste total",
                "DUMMY_FARIA_LIMA" = 0,
                "TOTAL_mean" = mean(dados_regressao$TOTAL_DIA[dados_regressao$REG8 == "Oeste"], na.rm = TRUE),
                "TOTAL_PICO_mean" = mean(dados_regressao$TOTAL_PICO[dados_regressao$REG8 == "Oeste"], na.rm = TRUE),
                "TOTAL_sd" = sd(dados_regressao$TOTAL_DIA[dados_regressao$REG8 == "Oeste"], na.rm = TRUE),
                "TOTAL_PICO_sd" = sd(dados_regressao$TOTAL_PICO[dados_regressao$REG8 == "Oeste"], na.rm = TRUE),
                "n" = nrow(dados_regressao %>% filter(REG8 == "Oeste")))

total <- tibble("REG8" = "Total",
                "DUMMY_FARIA_LIMA" = 0,
                "TOTAL_mean" = mean(dados_regressao$TOTAL_DIA, na.rm = TRUE),
                "TOTAL_PICO_mean" = mean(dados_regressao$TOTAL_PICO, na.rm = TRUE),
                "TOTAL_sd" = sd(dados_regressao$TOTAL_DIA, na.rm = TRUE),
                "TOTAL_PICO_sd" = sd(dados_regressao$TOTAL_PICO, na.rm = TRUE),
                "n" = nrow(dados_regressao))

medias_por_regiao <- dados_regressao %>% 
  group_by(REG8, DUMMY_FARIA_LIMA) %>% 
  summarise(TOTAL_mean = mean(TOTAL_DIA, na.rm = TRUE),
            TOTAL_PICO_mean = mean(TOTAL_PICO, na.rm = TRUE),
            TOTAL_sd = sd(TOTAL_DIA, na.rm = TRUE),
            TOTAL_PICO_sd = sd(TOTAL_PICO, na.rm = TRUE),
            n = n()) %>% 
  ungroup() %>% 
  rbind(oeste, total)

write.csv(medias_por_regiao, 
          file = paste0(dir_path, "dados/output/medias_por_regiao.csv"))

# médias por região pré e pandemia ----
medias_por_regiao_pandemia <- dados_regressao %>% 
  group_by(REG8, DUMMY_PANDEMIA) %>% 
  summarise(TOTAL_mean = mean(TOTAL_DIA, na.rm = TRUE),
            TOTAL_PICO_mean = mean(TOTAL_PICO, na.rm = TRUE),
            TOTAL_sd = sd(TOTAL_DIA, na.rm = TRUE),
            TOTAL_PICO_sd = sd(TOTAL_PICO, na.rm = TRUE),
            n = n()) 

write.csv(medias_por_regiao, 
          file = paste0(dir_path, "dados/output/medias_por_regiao_pandemia.csv"))

# pontos e observações por região ----
obs_por_reg <- dados_regressao %>% 
  count(REG8, ID_PTOS) %>% 
  add_count(REG8, name = "pontos_por_reg") %>% 
  group_by(REG8) %>% 
  mutate(obs_por_reg = sum(n)) %>% 
  ungroup() %>% 
  mutate(prov = ifelse(ID_PTOS %in% c(35, 34, 4), 0, 1)) %>% 
  group_by(REG8) %>% 
  mutate(obs_por_reg_sem_auto = sum(n*prov)) %>% 
  ungroup() %>% 
  select(-prov)

dias_por_reg <- dados_regressao %>% 
  count(REG8, DATA) %>% 
  count(REG8, name = "dias_por_reg")

dias_por_reg_sem_auto <- dados_regressao %>% 
  filter(ID_PTOS %notin% c(35, 34, 4)) %>% 
  count(REG8, DATA) %>% 
  count(REG8, name = "dias_por_reg_sem_auto")

obs_por_reg <- obs_por_reg %>% 
  left_join(dias_por_reg, by = "REG8") %>% 
  left_join(dias_por_reg_sem_auto, by = "REG8")

write.csv(obs_por_reg, 
          file = paste0(dir_path, "dados/output/obs_e_pontos_por_regiao.csv"))

#############################
#### MODELO DE REGRESSÃO ####
#############################
dados_regressao <- dados_regressao %>% 
  select(-CONTAGEM, -PCT_FAIXAS_HORARIAS)

lm_formula <- paste0(paste(controles, collapse="+"))
lm_formula_socio <- paste0(paste(socio, collapse="+"))

# OLS sem controles ----
lm_simples <- lm(as.formula(paste0(var_dep, "~ ", vari_indep)), 
                 data = dados_regressao)

coeftest_simples <- coeftest(lm_simples, 
                             vcovCL(lm_simples, cluster = ~ ID_PTOS, type = "HC1"), #HC1 is Stata default
                             save = TRUE) 

# partindo de 453 ciclistas, cada 1 ciclista adicional na hora de pico conta como 1.6 ciclistas para o dia todo.
# summary(lm_simples)

# OLS com dummies ----
lm_dummies <- lm(as.formula(TOTAL_DIA ~ TOTAL_PICO*DUMMY_YELLOW + 
                              TOTAL_PICO*DUMMY_PANDEMIA + 
                              TOTAL_PICO*DUMMY_FARIA_LIMA), 
                 data = dados_regressao)

coeftest_dummies <- coeftest(lm_dummies, 
                             vcovCL(lm_dummies, cluster = ~ ID_PTOS, type = "HC1"), #HC1 is Stata default
                             save = TRUE) 

# summary(lm_dummies)

# OLS com regioes ----
lm_regioes <- lm(as.formula(TOTAL_DIA ~ TOTAL_PICO*DUMMY_YELLOW + 
                              TOTAL_PICO*DUMMY_PANDEMIA + 
                              TOTAL_PICO*DUMMY_FARIA_LIMA +
                            TOTAL_PICO*REG8), 
                 data = dados_regressao)

coeftest_regioes <- coeftest(lm_regioes, 
                             vcovCL(lm_regioes, cluster = ~ ID_PTOS, type = "HC1"), #HC1 is Stata default
                             save = TRUE) 

# summary(lm_regioes)

# OLS com socioeconomicas ----
lm_socio <- lm(as.formula(paste0("TOTAL_DIA ~ TOTAL_PICO*DUMMY_YELLOW + 
                            TOTAL_PICO*DUMMY_PANDEMIA + 
                            TOTAL_PICO*DUMMY_FARIA_LIMA +
                              TOTAL_PICO*REG8 +", lm_formula_socio)), 
                 data = dados_regressao)

coeftest_socio <- coeftest(lm_socio, 
                           vcovCL(lm_socio, cluster = ~ ID_PTOS, type = "HC1"), #HC1 is Stata default
                           save = TRUE)

# summary(lm_socio)

# tudo ----
lm_try1 <- lm(as.formula(paste0("TOTAL_DIA ~ TOTAL_PICO*DUMMY_YELLOW + 
                                TOTAL_PICO*DUMMY_PANDEMIA + TOTAL_PICO*DUMMY_FARIA_LIMA + TOTAL_PICO*REG8 +", 
                                lm_formula)), 
               data = dados_regressao)

coeftest1 <- coeftest(lm_try1, 
                      vcovCL(lm_try1, cluster = ~ ID_PTOS, type = "HC1"), #HC1 is Stata default
                      save = TRUE) 

# summary dos modelos ----
summary(lm_simples)
summary(lm_dummies)
summary(lm_regioes)
summary(lm_socio)
summary(lm_try1)


##############################
#### PREVISAO DOS MODELOS ####
##############################
# previsão de cada modelo e plots ----
list_modelos <- list(lm_simples, lm_dummies, lm_regioes, lm_socio, lm_try1)
nomes_modelos <- c("Simples", "Add dummies", "Add regiões", "Add socioeconômicas", "Tudo")
list_df <- list()

for (i in seq_along(nomes_modelos)) {
  list_df[[i]] <- as_tibble(predict(list_modelos[[i]], new.data = dados_regressao, interval = "confidence")) %>% 
    mutate(ID = row_number(),
           modelo = nomes_modelos[i])
}

previsao_modelos <- bind_rows(list_df)

datas_ID_PTOS <- dados_regressao %>% 
  select(DATA, ID_PTOS, REG8) %>% 
  mutate(ID = row_number())

dados_ID_PTOS <- dados_regressao %>% 
  select(TOTAL_DIA) %>% 
  mutate(ID = row_number(),
         modelo = "Observado",
         lwr = TOTAL_DIA,
         upr = TOTAL_DIA) %>% 
  rename(fit = TOTAL_DIA)

previsao_modelos <- previsao_modelos %>% 
  rbind(dados_ID_PTOS) %>% 
  left_join(datas_ID_PTOS, by = "ID") %>% 
  mutate(modelo = factor(modelo, levels = c("Observado", "Simples", "Add dummies", 
                                            "Add regiões", "Add socioeconômicas", "Tudo")))

# gráfico comparando as previsões dos modelos ----
tema_grafico <- theme_classic() +
  theme(plot.title = element_text(hjust = 0.5, size = 12), 
        plot.subtitle = element_text(hjust = 0.5, size = 10),
        axis.ticks.x=element_blank(),
        legend.position = "right", legend.box="vertical", legend.margin=margin(),
        axis.title.x = element_text(size = 8),
        axis.title.y = element_text(size = 8))

tema_grafico2 <- theme_classic() +
  theme(plot.title = element_text(hjust = 0, size = 10), 
        plot.subtitle = element_text(hjust = 0, size = 10),
        axis.text.x = element_text(hjust = 0.5, size = 6),
        legend.position = "bottom", legend.box="vertical",
        legend.margin=margin(), legend.text = element_text(size = 8),
        axis.title.x = element_text(size = 8),
        axis.title.y = element_text(size = 8))

tema_grafico3 <- theme_classic() +
  theme(plot.title = element_text(hjust = 0, size = 10), 
        plot.subtitle = element_text(hjust = 0, size = 10),
        axis.text.x = element_text(angle = 60, hjust = 1, size = 6),
        axis.ticks.x=element_blank(),
        legend.position = "bottom", legend.box="vertical", 
        legend.margin=margin(),legend.text = element_text(size = 8),
        axis.title.x = element_text(size = 8),
        axis.title.y = element_text(size = 8))

cores_graficos6 <- c("#bdbdbd", "#de2d26", "#fdae6b", "#7fcdbb", "#2c7fb8", "black", '#e0f3f8', '#ae017e')
cores_graficos8 <- c("#bdbdbd", "#de2d26", "#fdae6b", "#7fcdbb", "#2c7fb8", '#ae017e', "black", '#e0f3f8')

nomes_IDs <- read_excel(paste0(dir_path,"dados/input/contagens/site_CET/nomes_IDs.xlsx"))

# previsão geral - série histórica ----
previsao_modelos %>% 
  group_by(DATA) %>% 
  mutate(fit = mean(fit, na.rm = TRUE),
         lwr = mean(lwr, na.rm = TRUE),
         upr = mean(upr, na.rm = TRUE),
         DATA = as.Date(DATA),
         ANO = str_sub(as.character(DATA), start = 1, end = 4)) %>%
  ggplot(aes(x = DATA, y = fit, fill = ANO)) + 
  geom_bar(stat="identity", position=position_dodge()) +
  geom_errorbar(aes(ymin=lwr, ymax=upr), width=.2,
                position=position_dodge(.9)) +
  #scale_y_continuous(labels = number_format())+
  scale_fill_manual(values = cores_graficos6,
                    name = "Ano") +
  scale_x_date(date_breaks = "3 months", date_labels = "%b")+
  facet_wrap(~modelo, ncol = 1) +
  labs(title = "",#paste0("Contagens no ponto ID ", id_pontos[i]),
       subtitle = paste0("Contagens estimadas pelos modelos de regressão"),
       x = "",
       y = "Contagem total no dia")+
  tema_grafico3

ggsave(plot = last_plot(),
       filename = paste0(dir_path, "figuras/results/reg_todos_os_modelos_contagens_previstas.png"),
       width = 14,
       height = 20)


# previsão por ponto ----
id_pontos <- dados_regressao %>% arrange(ID_PTOS) %>% distinct(ID_PTOS) %>% 
  mutate(ID_PTOS = gsub("-automáticas", "", ID_PTOS),
         ID_PTOS = as.numeric(ID_PTOS)) %>% 
  pull(ID_PTOS)

previsao_modelos <- previsao_modelos %>% 
  mutate(ID_PTOS = gsub("-automáticas", "", ID_PTOS),
         ID_PTOS = as.numeric(ID_PTOS))

for (i in seq_along(id_pontos)) {
  nome_aux <- nomes_IDs %>% filter(ID_PTOS == id_pontos[i]) %>% pull(NOME_ID)
  
  if (id_pontos[i] %in% c(34, 35)) {
    previsao_modelos %>% 
      filter(ID_PTOS == id_pontos[i]) %>% 
      mutate(DATA = as.Date(DATA),
             ANO = str_sub(as.character(DATA), start = 1, end = 4)) %>%
      ggplot(aes(x = DATA, y = fit, fill = ANO)) + 
      geom_bar(stat="identity", position=position_dodge()) +
      geom_errorbar(aes(ymin=lwr, ymax=upr), width=.2,
                    position=position_dodge(.9)) +
      #scale_y_continuous(labels = number_format())+
      scale_fill_manual(values = cores_graficos6,
                        name = "Ano") +
      scale_x_date(date_breaks = "3 months", date_labels = "%b")+
      facet_wrap(~modelo, ncol = 2) +
      labs(title = "",#paste0("Contagens no ponto ID ", id_pontos[i]),
           subtitle = paste0("Contagens estimadas no ponto ID ", id_pontos[i], " - ", nome_aux),
           x = "",
           y = "Contagem total no dia")+
      tema_grafico2

  } else{
    previsao_modelos %>% 
      filter(ID_PTOS == id_pontos[i]) %>% 
      mutate(DATA_FAC = factor(format(DATA, format = '%Y-%m-%d')),
             ANO = str_sub(as.character(DATA), start = 1, end = 4)) %>% 
      ggplot(aes(x = DATA_FAC, y = fit, fill = ANO)) + 
      geom_bar(stat="identity", position=position_dodge()) +
      geom_errorbar(aes(ymin=lwr, ymax=upr), width=.2,
                    position=position_dodge(.9)) +
      #scale_y_continuous(labels = number_format())+
      scale_fill_manual(values = cores_graficos6,
                        name = "Ano") +
      facet_wrap(~modelo, ncol = 2) +
      labs(title = "",
           subtitle = paste0("Contagens estimadas no ponto ID ", id_pontos[i], " - ", nome_aux),
           x = "",
           y = "Contagem total no dia")+
      tema_grafico3
  }
  
  ggsave(plot = last_plot(),
         filename = paste0(dir_path, "figuras/results/regressoes_faixa_horaria/reg_faixas_hor_contagens_ID_", 
                           as.character(id_pontos[i]), ".png"),
         width = 14,
         height = 12)
}

# previsão por região ----
regioes <- previsao_modelos %>% distinct(REG8) %>% pull(REG8)

for (i in seq_along(regioes)) {
  previsao_modelos %>% 
    filter(REG8 == regioes[i]) %>% 
    group_by(DATA, modelo) %>% 
    mutate(fit = mean(fit),
           lwr = mean(lwr),
           upr = mean(upr),
           ANO = str_sub(as.character(DATA), start = 1, end = 4),
           DATA_FAC = factor(format(DATA, format = '%Y-%m-%d'))) %>% 
    ggplot(aes(x = DATA_FAC, y = fit, fill = ANO)) + 
    geom_bar(stat="identity", position=position_dodge()) +
    geom_errorbar(aes(ymin=lwr, ymax=upr), width=.2,
                  position=position_dodge(.9)) +
    #scale_y_continuous(labels = number_format())+
    scale_fill_manual(values = cores_graficos6,
                      name = "Ano") +
    facet_wrap(~modelo, ncol = 2) +
    labs(title = "",#paste0("Contagens no ponto ID ", id_pontos[i]),
         subtitle = paste0("Contagens estimada na região ", regioes[i]),
         x = "",
         y = "Contagem total no dia")+
    tema_grafico3
  
  ggsave(plot = last_plot(),
         filename = paste0(dir_path, "figuras/results/regressoes_faixa_horaria/reg_faixas_hor_contagens_regiao_", 
                           as.character(regioes[i]), ".png"),
         width = 14,
         height = 10)
}

# scatter plot comparando regiões -----
previsao_modelos <- bind_rows(list_df)

datas_ID_PTOS <- dados_regressao %>% 
  select(DATA, ID_PTOS, REG8, TOTAL_DIA, TOTAL_PICO) %>% 
  mutate(ID = row_number())

previsao_modelos <- previsao_modelos %>% 
  left_join(datas_ID_PTOS, by = "ID") %>% 
  mutate(modelo = factor(modelo, levels = c("Simples", "Add dummies", 
                                            "Add regiões", "Add socioeconômicas", "Tudo")))

previsao_modelos %>% 
  filter(modelo == "Simples") %>% 
  group_by(DATA, modelo) %>% 
  mutate(fit = mean(fit),
         lwr = mean(lwr),
         upr = mean(upr),
         TOTAL_DIA = mean(TOTAL_DIA), 
         TOTAL_PICO = mean(TOTAL_PICO),
         ANO = str_sub(as.character(DATA), start = 1, end = 4),
         DATA_FAC = factor(format(DATA, format = '%Y-%m-%d'))) %>% 
  ggplot(aes(x = TOTAL_PICO, y = TOTAL_DIA, color = REG8)) + 
  geom_point(size = 0.2) +
  stat_smooth(method = lm) +
  scale_color_manual(values = cores_graficos8,
                    name = "Região")+
  labs(subtitle = "Relação entre contagem das faixas horárias e total do dia",
       x = "Contagem nas faixas horárias selecionadas (7, 8, 9, 17, 18, 19)",
       y = "Contagem total no dia")+
  tema_grafico

ggsave(plot = last_plot(),
       filename = paste0(dir_path, "figuras/results/regressoes_faixa_horaria/reg_faixas_hor_contagens_regiao.png"))
  
previsao_modelos %>% 
  filter(modelo == "Simples") %>% 
  group_by(DATA, modelo) %>% 
  mutate(fit = mean(fit),
         lwr = mean(lwr),
         upr = mean(upr),
         TOTAL_DIA = mean(TOTAL_DIA), 
         TOTAL_PICO = mean(TOTAL_PICO),
         ANO = str_sub(as.character(DATA), start = 1, end = 4),
         DATA_FAC = factor(format(DATA, format = '%Y-%m-%d'))) %>% 
  ggplot(aes(x = TOTAL_PICO, y = TOTAL_DIA, color = REG8)) + 
  geom_point(size = 0.2) +
  stat_smooth(method = lm) +
  scale_color_manual(values = cores_graficos8,
                     name = "Região")+
  facet_wrap(~REG8) +
  labs(subtitle = "Relação entre contagem das faixas horárias e total do dia",
       x = "Contagem nas faixas horárias selecionadas (7, 8, 9, 17, 18, 19)",
       y = "Contagem total no dia")+
  tema_grafico

ggsave(plot = last_plot(),
       filename = paste0(dir_path, "figuras/results/regressoes_faixa_horaria/reg_faixas_hor_contagens_regiao_wraped.png"),
       width = 10,
       height = 10)

previsao_modelos %>% 
  filter(modelo == "Tudo") %>% 
  group_by(DATA, modelo) %>% 
  mutate(fit = mean(fit),
         lwr = mean(lwr),
         upr = mean(upr),
         TOTAL_DIA = mean(TOTAL_DIA), 
         TOTAL_PICO = mean(TOTAL_PICO),
         ANO = str_sub(as.character(DATA), start = 1, end = 4),
         DATA_FAC = factor(format(DATA, format = '%Y-%m-%d'))) %>% 
  ggplot(aes(x = TOTAL_PICO, y = TOTAL_DIA)) + 
  geom_point(size = 0.2) +
  stat_smooth(method = lm) +
  labs(subtitle = "Relação entre contagem das faixas horárias e total do dia",
       x = "Contagem nas faixas horárias selecionadas (7, 8, 9, 17, 18, 19)",
       y = "Contagem total no dia")+
  tema_grafico

ggsave(plot = last_plot(),
       filename = paste0(dir_path, "figuras/results/regressoes_faixa_horaria/scatter_plot_regressao_simples.png"))

########################################################
#### SALVANDO TODOS OS MODELOS EM UM ÚNICO XLSX ########
########################################################
# salvando o resultado das regressões ----
# https://cran.r-project.org/web/packages/huxtable/vignettes/huxreg.html
regressoes <- as.data.frame(
  add_footnote(
    huxreg("Simples" = coeftest_simples, 
           "Add dummies" = coeftest_dummies, 
           "Add regiôes" = coeftest_regioes,
           "Add socioeconômicas" = coeftest_socio,
           "Tudo" = coeftest1,
           bold_signif = 0.05),
  "Nota: erros robustos clusterizados a nível de ponto."))

write.csv(regressoes,
          file = paste0(dir_path, "dados/output/resultado_modelos_regressao.csv"))

# salvando o objeto com as previsões de cada modelo. ----
write.csv(previsao_modelos,
          file = paste0(dir_path, "dados/output/previsao_modelos_regressao.csv"))


##################################
#### TESTE SIMULADOR DE EXCEL ####
##################################
nomes_abas <- c("Centro", "Leste 1", "Leste 2", "Norte 1", "Norte 2", "Oeste", "Sul 1", "Sul 2")
for (i in seq_along(nomes_abas)) {
  df <- read_excel(paste0(dir_path, "relatorio_final/teste_simulador.xlsx"), 
                   sheet = nomes_abas[i])
  
  df <- df %>% 
    mutate(VALOR_PREVISTO = stats::predict(lm_regioes, newdata = ., interval = "prediction")[,"fit"],
           VALOR_PREVISTO_INF = stats::predict(lm_regioes, newdata = ., interval = "prediction")[,"lwr"],
           VALOR_PREVISTO_SUP = stats::predict(lm_regioes, newdata = ., interval = "prediction")[,"upr"],
           VALOR_CONF = stats::predict(lm_regioes, newdata = ., interval = "confidence")[,"fit"],
           VALOR_CONF_INF = stats::predict(lm_regioes, newdata = ., interval = "confidence")[,"lwr"],
           VALOR_CONF_SUP = stats::predict(lm_regioes, newdata = ., interval = "confidence")[,"upr"])

    # write.xlsx(df,
    #            file = paste0(dir_path, "relatorio_final/teste_simulador_output.xlsx"),
    #            sheetName = nomes_abas[i], append=TRUE)
    # 
    # print(i)
  
  write.csv(df,
             file = paste0(dir_path, "relatorio_final/teste_simulador_", nomes_abas[i], ".csv"))
  
  remove(df)
  
}

remove(nomes_abas, i)
#######################################
#### LIMPANDO AMBIENTE DE TRABALHO ####
#######################################
rm(list = setdiff(ls(),
                  c("contagens_auto", "localizacao", "dir_path", 
                    "%notin%", "filter", "rm_accent", "select",
                    "source", 
                    "previsao_modelos", "regressoes")))

gc()