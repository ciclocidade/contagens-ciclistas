# ####################################################################### #
####                       ABRINDO OS DADOS                            ####
# ####################################################################### #
# listando arquivos salvos em padrão ".csv" das contagens para ler
files_xls <- grep(
  list.files(
    path = paste0(dir_path, "2023_contagem_linha_base/dados/input/CET_manuais/"), 
    all.files = FALSE,
    full.names = TRUE
  ), 
  pattern=".xlsx",      # arquivos que não devem ser listados
  inv=FALSE, 
  value=TRUE)

# abrindo arquivos de contagens móveis
class_colunas <-  c("text", "text", "text", "text", 
                    "date", 
                    "numeric", "numeric", 
                    "text", "text", "text", 
                    "numeric", "numeric", "numeric", 
                    "text", "text", "text", "text", "text")

manuais <- files_xls %>% 
  map_df(read_excel, 
         col_types = class_colunas)

remove(class_colunas, files_xls)

df_N <- tibble("base" = "base original",
               "tamanho" = nrow(manuais))

names(manuais)

# ####################################################################### #
####                       LIMPANDO OS DADOS                           ####
# ####################################################################### #
# criar variável faixa-hora e hora-pico ----
manuais <- manuais %>% 
  mutate(FAIXA_HORA = as.numeric( 
           case_when(
             str_length(hora) %in% c(1, 2) ~ hora,
             str_length(hora) > 2 ~ str_sub(start = 1, end = 2, hora))),
         HORA_PICO = ifelse(FAIXA_HORA %in% c(7, 8, 9, 16, 17, 18, 19), 1, 0)) 
# %>% 
#   group_by(DATA, ID_PTOS) %>%                                                  
#   summarise(TOTAL_PICO =  sum(TOTAL * HORA_PICO, na.rm = TRUE), 
#             TOTAL_MANHA = sum(TOTAL * HORA_PICO_MANHA, na.rm = TRUE),
#             TOTAL_TARDE = sum(TOTAL * HORA_PICO_TARDE, na.rm = TRUE),             
#             TOTAL_DIA =   sum(TOTAL, na.rm = TRUE)))

table(manuais$FAIXA_HORA)

# manter apenas hora-pico ----
manuais <- manuais %>% 
  filter(HORA_PICO == 1)

df_N <- df_N %>% 
  rbind(tibble("base" = "mantendo apenas hora pico (7, 8, 9, 16, 17, 18, 19)",
               "tamanho" = nrow(manuais)))

manuais <- manuais %>% 
  mutate(ID_PTOS = gsub("[^0-9.-]", "", ID_PTOS),
         ID_PTOS = gsub("-", "", ID_PTOS),
         ID_PTOS = paste(ID_PTOS, "manuais", sep = "-"))

# agregando contagem por id-ponto, data e faixa-hora ----
manuais_agreg <- manuais  %>% 
  mutate(FAIXA_HORA = ifelse(FAIXA_HORA == 19, 18, FAIXA_HORA)) %>% 
  group_by(ID_PTOS, local, data, FAIXA_HORA) %>% #, estrutura_cicloviaria) %>% 
  summarise(Volume_total = sum(Volume_total, na.rm = TRUE))

df_N <- df_N %>% 
  rbind(tibble("base" = "agregando contagem por id-ponto, data e faixa-hora",
               "tamanho" = nrow(manuais_agreg)))

# excluindo contagens com menos de 3 horas pela manhã ou pela tarde ----
manuais_agreg <- manuais_agreg %>% 
  mutate(HORA_PICO = ifelse(FAIXA_HORA %in% c(7, 8, 9, 16, 17, 18, 19), 1, 0),
         HORA_PICO_MANHA = ifelse(FAIXA_HORA %in% c(7, 8, 9), 1, 0),
         HORA_PICO_TARDE = ifelse(FAIXA_HORA %in% c(16, 17, 18, 19), 1, 0)) %>% 
  group_by(ID_PTOS, local, data) %>% 
  mutate(n = n(),
         n_manha = sum(HORA_PICO_MANHA),
         n_tarde = sum(HORA_PICO_TARDE)) %>% 
  filter((n_manha == 3 | n_tarde == 3) & (n == 3 | n == 6))

table(manuais_agreg$n)
table(manuais_agreg$n_manha)
table(manuais_agreg$n_tarde)

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo contagens com menos de 3 horas pela manhã ou pela tarde",
               "tamanho" = nrow(manuais_agreg)))

# agrupando por id-ponto e data ----
manuais_agreg <- manuais_agreg %>% 
  group_by(ID_PTOS, data) %>% 
  summarise(TOTAL_PICO = sum(Volume_total * HORA_PICO, na.rm = TRUE),
            TOTAL_MANHA = sum(Volume_total * HORA_PICO_MANHA, na.rm = TRUE),
            TOTAL_TARDE = sum(Volume_total * HORA_PICO_TARDE, na.rm = TRUE)) %>% 
  mutate(TOTAL_DIA = NA_integer_) %>% 
  rename(DATA = data)

df_N <- df_N %>% 
  rbind(tibble("base" = "agrupando por id-ponto e data",
               "tamanho" = nrow(manuais_agreg)))

# excluindo finais de semana ----
manuais_agreg <- manuais_agreg %>% 
  mutate(DIA_SEMANA = weekdays(DATA)) %>% 
  filter(DIA_SEMANA %notin% c("sábado", "domingo"))

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo finais de semana",
               "tamanho" = nrow(manuais_agreg)))

# excluindo feriados ----
feriados_nac <- read_excel(paste0(dir_path, "dados/input/feriados/feriados_nacionais.xls"), 
                           col_types = c("date", "text", "text")) %>%
  select(-`Dia da Semana`) %>%
  rename(DATA = Data)

feriados_sp <- tibble("DATA" = c("2016-07-09", "2016-11-20", "2016-01-25",
                                 "2017-07-09", "2017-11-20", "2017-01-25",
                                 "2018-07-09", "2018-11-20", "2018-01-25",
                                 "2019-07-09", "2019-11-20", "2019-01-25",
                                 "2020-07-09", "2020-11-20", "2020-01-25",
                                 "2021-07-09", "2021-11-20", "2021-01-25",
                                 "2022-07-09", "2022-11-20", "2022-01-25",
                                 "2023-07-09", "2023-11-20", "2023-01-25"),
                      "Feriado" = rep(
                        c("Revolução constitucionalista", 
                          "Consciência negra", 
                          "Aniversário cidade SP"),
                        times = 8)) %>%
  mutate(DATA = as.Date(DATA))

feriados <- rbind(feriados_nac, feriados_sp) %>%                    # une feriados nacionais com feriados de SP
  mutate(FERIADO = 1) %>%
  select(-Feriado)

manuais_agreg <- manuais_agreg %>%
  left_join(feriados, by = "DATA") %>%
  filter(is.na(FERIADO)) %>% 
  select(-FERIADO)

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo feriados",
               "tamanho" = nrow(manuais_agreg)))

remove(feriados, feriados_sp, feriados_nac)

# ####################################################################### #
####                          SALVAR BASES                             ####
# ####################################################################### #
write.csv2(df_N, 
           paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_manuais_df_N.csv"))
remove(df_N)

write.csv2(manuais, 
           paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_manuais_LIMPA.csv"))

write.csv2(manuais_agreg, 
           paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_manuais_LIMPA_AGREGADA.csv"))



# ####################################################################### #
####          CRIAR LOCALIZAÇÃO PARA CONTAGENS MANUAIS                 ####
# ####################################################################### #
localizacao_manuais <- manuais %>% 
  select(ID_PTOS, longitude, latitude) %>% 
  distinct(ID_PTOS, .keep_all = TRUE) %>% 
  st_as_sf(coords = c("longitude", "latitude"), crs = 4326) %>% 
  filter(ID_PTOS %in% manuais_agreg$ID_PTOS)

saveRDS(localizacao_manuais,
        paste0(
          dir_path, 
          "2023_contagem_linha_base/dados/intermediate/localizacao_manuais.RDS"))

remove(manuais)

gc()
