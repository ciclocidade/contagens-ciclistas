# ####################################################################### #
####                       ABRINDO OS DADOS                            ####
# ####################################################################### #
# listando arquivos salvos em padrão ".csv" das contagens para ler
files_csv <- grep(
  list.files(
    path = paste0(dir_path, "2023_contagem_linha_base/dados/input/CET_automaticas/"), 
    all.files = FALSE,
    full.names = TRUE
  ), 
  pattern="_Tabela_VDM_calculado",      # arquivos que não devem ser listados
  inv=TRUE, 
  value=TRUE)

# abrindo arquivos de contagens móveis
contagens_auto <- files_csv %>% 
  map_df(fread, sep = ";", colClasses = 'character')

remove(files_csv)

# df para salvar o tamanho da base
df_N <- tibble("base" = "base original",
               "tamanho" = nrow(contagens_auto))

# ####################################################################### #
####                     ENTENDENDO OS DADOS                           ####
# ####################################################################### #
contagens_auto <- contagens_auto %>% 
  select(DATA_STR, FAIXA_HORA, TOTAL, ANO, ID_PTOS) %>% 
  mutate(ID_PTOS = as.character(ID_PTOS),
         DATA = as.Date(DATA_STR),
         DIA_SEMANA = weekdays(DATA),
         FAIXA_HORA = as.numeric(FAIXA_HORA), 
         TOTAL = as.numeric(TOTAL),
         ANO = factor(ANO, levels = c(as.character(2016:2023))))

# verificando NA ----
contagens_na <- contagens_auto %>% filter(is.na(TOTAL))
table(contagens_na$DATA, contagens_na$ID_PTOS)
write.csv2(contagens_na, 
           file = paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_automaticas_NA.csv"))
remove(contagens_na)

# verificando zeros ----
contagens_zero <- contagens_auto %>% filter(TOTAL == 0)

table(contagens_zero$FAIXA_HORA)

faixas_horarias <- contagens_zero %>% 
  arrange(FAIXA_HORA) %>% distinct(FAIXA_HORA) %>% pull(FAIXA_HORA)

# gráfico geral
contagens_zero %>% 
  mutate(`Excluída` = ifelse(FAIXA_HORA %in% c(0, 1, 2, 3, 4, 5, 22, 23), "Não", "Sim")) %>% 
  ggplot(aes( x = FAIXA_HORA, fill = `Excluída`)) +
  geom_histogram(binwidth=1,
                 color="black", size = 0.2)+
  scale_x_continuous("Faixas horárias", 
                     labels = as.character(faixas_horarias), 
                     breaks = faixas_horarias)+
  scale_fill_manual(values = c("#cccccc", "#525252")) +
  ylab("Número de observações") +
  theme_classic() +
  theme(legend.position = "bottom")

ggsave(plot = last_plot(),
       filename = paste0(
         dir_path, 
         "2023_contagem_linha_base/figuras/intermediate/histograma_contagens_zero.png"),
       width = 20,
       height = 15,
       units = "cm")

# gráfico só com as observações que serão excluídas
contagens_zero %>% 
  mutate(`Excluída` = ifelse(FAIXA_HORA %in% c(0, 1, 2, 3, 4, 5, 22, 23), "Não", "Sim")) %>%
  filter(`Excluída` == "Sim") %>% 
  ggplot(aes( x = FAIXA_HORA, fill = `Excluída`)) +
  geom_histogram(binwidth=1,
                 color="black", size = 0.2)+
  scale_x_continuous("Faixas horárias", 
                     labels = as.character(faixas_horarias), 
                     breaks = faixas_horarias)+
  scale_fill_manual(values = c("#cccccc", "#525252")) +
  labs(title = "Contagens zero excluídas", 
       subtitle = "Entre 6 e 21 horas",
        y ="Número de observações") +
  theme_classic() +
  theme(legend.position = "none")

ggsave(plot = last_plot(),
       filename = paste0(
         dir_path, 
         "2023_contagem_linha_base/figuras/intermediate/histograma_contagens_zero_excluidas.png"),
       width = 20,
       height = 15,
       units = "cm")

write.csv2(contagens_zero, 
           file = paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_automaticas_ZERO.csv"))

remove(contagens_zero, faixas_horarias)

# qual o primeiro e último dia da base? ----
# Preciso dessa informação para pedir os dados de chuva e temperatura
min(contagens_auto$DATA) # 18/01/2016
max(contagens_auto$DATA) # 10/05/2023

# ####################################################################### #
####                       LIMPANDO OS DADOS                           ####
# ####################################################################### #
# 1. excluir NA
# 2. excluir contagens zero entre 6h e 21h.
# 3. excluir finais de semana
# 4. excluir feriados

# ecluir NA ----
contagens_auto <- contagens_auto %>% 
  filter(!is.na(TOTAL))

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo NA",
               "tamanho" = nrow(contagens_auto)))

# exluir zeros entre 6 e 21h ----
contagens_auto <- contagens_auto %>% 
  filter(TOTAL > 0 | (TOTAL == 0 & FAIXA_HORA %in% c(0, 1, 2, 3, 4, 5, 22, 23)))

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo zeros entre 6 e 21h (inclusive)",
               "tamanho" = nrow(contagens_auto)))

# excluir finais de semana ----
contagens_auto <- contagens_auto %>%  
  filter(DIA_SEMANA %notin% c("sábado", "domingo"))

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo finais de semana",
               "tamanho" = nrow(contagens_auto)))

# excluir feriados ----
# ANBIMA: https://www.anbima.com.br/feriados/feriados.asp 
# https://github.com/joaopbini/feriados-brasil e 
# https://terminaldeinformacao.com/2017/06/28/tabela-todos-os-feriados-brasileiros/
feriados_nac <- read_excel(paste0(dir_path, "dados/input/feriados/feriados_nacionais.xls"), 
                           col_types = c("date", "text", "text")) %>%
  select(-`Dia da Semana`) %>%
  rename(DATA = Data)

feriados_sp <- tibble("DATA" = c("2016-07-09", "2016-11-20", "2016-01-25",
                                 "2017-07-09", "2017-11-20", "2017-01-25",
                                 "2018-07-09", "2018-11-20", "2018-01-25",
                                 "2019-07-09", "2019-11-20", "2019-01-25",
                                 "2020-07-09", "2020-11-20", "2020-01-25",
                                 "2021-07-09", "2021-11-20", "2021-01-25",
                                 "2022-07-09", "2022-11-20", "2022-01-25",
                                 "2023-07-09", "2023-11-20", "2023-01-25"),
                      "Feriado" = rep(
                        c("Revolução constitucionalista", 
                          "Consciência negra", 
                          "Aniversário cidade SP"),
                        times = 8)) %>%
  mutate(DATA = as.Date(DATA))

feriados <- rbind(feriados_nac, feriados_sp) %>%                    # une feriados nacionais com feriados de SP
  mutate(FERIADO = 1) %>%
  select(-Feriado)

contagens_auto <- contagens_auto %>%
  left_join(feriados, by = "DATA") %>%
  filter(is.na(FERIADO)) %>% 
  select(-FERIADO)

df_N <- df_N %>% 
  rbind(tibble("base" = "excluindo feriados",
               "tamanho" = nrow(contagens_auto)))

remove(feriados, feriados_sp, feriados_nac)

# ####################################################################### #
####                 AGREGAR A BASE POR DIA E PONTO                    ####
# ####################################################################### #
automaticas_agreg <- contagens_auto %>% 
  mutate(HORA_PICO = ifelse(FAIXA_HORA %in% c(7, 8, 9, 17, 18, 19), 1, 0),
         HORA_PICO_MANHA = ifelse(FAIXA_HORA %in% c(7, 8, 9), 1, 0),
         HORA_PICO_TARDE = ifelse(FAIXA_HORA %in% c(17, 18, 19), 1, 0)) %>% 
  group_by(DATA, ID_PTOS) %>%                                                  
  summarise(TOTAL_PICO =  sum(TOTAL * HORA_PICO, na.rm = TRUE), 
            TOTAL_MANHA = sum(TOTAL * HORA_PICO_MANHA, na.rm = TRUE),
            TOTAL_TARDE = sum(TOTAL * HORA_PICO_TARDE, na.rm = TRUE),             
            TOTAL_DIA =   sum(TOTAL, na.rm = TRUE)) %>%                             
  ungroup()

df_N <- df_N %>% 
  rbind(tibble("base" = "agregando por hora pico (7, 8, 9, 17, 18, 19)",
               "tamanho" = nrow(automaticas_agreg)))

# ####################################################################### #
####                          SALVAR BASES                             ####
# ####################################################################### #
write.csv2(df_N, 
           paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_automaticas_df_N.csv"))
remove(df_N)

write.csv2(contagens_auto, 
           paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_automaticas_LIMPA.csv"))
remove(contagens_auto)

write.csv2(automaticas_agreg, 
           paste0(
             dir_path, 
             "2023_contagem_linha_base/dados/intermediate/contagens_automaticas_LIMPA_AGREGADA.csv"))


# ####################################################################### #
####     PLANILHA DE LOCALIZAÇÃO - GEORREFERENCIAMENTO DOS PONTOS      ####
# ####################################################################### #
# abrir a planilha com as localizações, 
# selecionar variáveis de interesse e 
# transformar em sf
localizacao <- 
  fread(
    paste0(
      dir_path, 
      "2023_contagem_linha_base/dados/input/CET_automaticas/_Tabela_VDM_calculado.csv"),
    sep = ";", colClasses = 'character') %>% 
  select(ID_PTOS, LAT, LONG) %>% 
  mutate(LAT = as.numeric(gsub(",", ".", LAT)),
         LONG = as.numeric(gsub(",", ".", LONG))) %>% 
  st_as_sf(coords = c("LONG", "LAT"), crs = 4326) # transformar o banco de dados de localização num objeto georreferenciado

saveRDS(localizacao,
        paste0(
          dir_path, 
          "2023_contagem_linha_base/dados/intermediate/localizacao_automaticas.RDS"))

gc()
