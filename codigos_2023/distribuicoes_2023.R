# PICO (MANHÃ E TARDE JUNTOS) ----
fit.gamma <- fitdist(contagens$TOTAL_PICO, distr = "gamma", method = "mme")
# plot(fit.gamma)

fit.exp <- fitdist(contagens$TOTAL_PICO, distr = "exp", method = "mme")
# plot(fit.exp)

fit.poi <- fitdist(contagens$TOTAL_PICO, distr = "pois", method = "mme")
# plot(fit.poi)

fit.nb <- fitdist(contagens$TOTAL_PICO, distr = "nbinom", method = "mme")

# https://stats.stackexchange.com/questions/240455/fitting-exponential-regression-model-by-mle


png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_nb_pico.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.nb)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_poi_pico.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.poi)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_exp_pico.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.exp)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_gamma_pico.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.gamma)
# 3. Close the file
dev.off()

# PICO MANHÃ ----
df_aux <- contagens %>% 
  filter(!is.na(TOTAL_MANHA)) %>% 
  filter(!is.null(TOTAL_MANHA))

fit.gamma <- fitdist(df_aux$TOTAL_MANHA, distr = "gamma", method = "mme")
# plot(fit.gamma)

fit.exp <- fitdist(df_aux$TOTAL_MANHA, distr = "exp", method = "mme")
# plot(fit.exp)

fit.poi <- fitdist(df_aux$TOTAL_MANHA, distr = "pois", method = "mme")
# plot(fit.poi)

fit.nb <- fitdist(df_aux$TOTAL_MANHA, distr = "nbinom", method = "mme")

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_nb_manha.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.nb)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_poi_manha.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.poi)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_exp_manha.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.exp)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_gamma_manha.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.gamma)
# 3. Close the file
dev.off()

# PICO TARDE ----
df_aux <- contagens %>% 
  filter(!is.na(TOTAL_TARDE)) %>% 
  filter(!is.null(TOTAL_TARDE))

fit.gamma <- fitdist(df_aux$TOTAL_TARDE, distr = "gamma", method = "mme")
# plot(fit.gamma)

fit.exp <- fitdist(df_aux$TOTAL_TARDE, distr = "exp", method = "mme")
# plot(fit.exp)

fit.poi <- fitdist(df_aux$TOTAL_TARDE, distr = "pois", method = "mme")
# plot(fit.poi)

fit.nb <- fitdist(df_aux$TOTAL_TARDE, distr = "nbinom", method = "mme")

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_nb_tarde.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.nb)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_poi_tarde.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.poi)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_exp_tarde.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.exp)
# 3. Close the file
dev.off()

png(filename = paste0(dir_path, 
                      "2023_contagem_linha_base/figuras/intermediate/distribuicao_gamma_tarde.png"), 
    width = 712, height = 408)
# 2. Create the plot
plot(fit.gamma)
# 3. Close the file
dev.off()


remove(fit.exp, fit.gamma, fit.nb, fit.poi)
gc()
