# ####################################################################### #
####                   JUNTANDO OS DADOS FINAIS                        ####
# ####################################################################### #
# localizacao ----
files_rds <- grep(
  list.files(
    path = paste0(dir_path, "2023_contagem_linha_base/dados/intermediate/"), 
    all.files = FALSE,
    full.names = TRUE), 
  pattern="localizacao_",      # arquivos que não devem ser listados
  inv=FALSE, 
  value=TRUE)

pontos_shp <- files_rds %>% 
  map_df(readRDS)

pontos_shp <- pontos_shp %>% 
  mutate(TIPO = 
           case_when(
             str_detect(ID_PTOS, "Z") ~ "linha de base",
             str_detect(ID_PTOS, "manuais") ~ "manuais",
             TRUE ~ "automáticas"),
         TIPO = factor(TIPO, levels = c("automáticas", "manuais", "linha de base")))

table(pontos_shp$TIPO)

saveRDS(pontos_shp,
        paste0(
          dir_path, 
          "2023_contagem_linha_base/dados/output/localizacao_ALL.RDS"))

# banco de dados ----
files_csv <- grep(
  list.files(
    path = paste0(dir_path, "2023_contagem_linha_base/dados/intermediate/"), 
    all.files = FALSE,
    full.names = TRUE), 
  pattern="LIMPA_AGREGADA",      # arquivos que não devem ser listados
  inv=FALSE, 
  value=TRUE)

pontos_df <- files_csv %>% 
  map_df(fread, sep = ";", colClasses = 'character') %>% 
  select(DATA, ID_PTOS, TOTAL_PICO, TOTAL_MANHA, TOTAL_TARDE) %>% 
  mutate(TIPO = 
           case_when(
             str_detect(ID_PTOS, "Z") ~ "linha de base",
             str_detect(ID_PTOS, "manuais") ~ "manuais",
             TRUE ~ "automáticas"),
         TIPO = factor(TIPO, levels = c("automáticas", "manuais", "linha de base")))

write.csv2(pontos_df,
           paste0(
             dir_path,
             "2023_contagem_linha_base/dados/output/pontos_ALL.csv"))

remove(files_csv, files_rds)

# ################################################ #
####               TABELAS RESUMO               ####
# ################################################ #
write.csv2(contagens %>% 
             mutate(ANO = str_sub(as.character(DATA), start = 1, end = 4)) %>% 
             count(ANO, TIPO) %>% 
             pivot_wider(names_from = TIPO, values_from = n) %>% 
             as_tibble(),
           paste0(
             dir_path,
             "2023_contagem_linha_base/dados/INTERMEDIATE/FINAL_contagens_por_ano.csv"))



write.csv2(contagens %>% 
             mutate(ANO = str_sub(as.character(DATA), start = 1, end = 4)) %>% 
             distinct(ID_PTOS, .keep_all = TRUE) %>% 
             count(ANO, TIPO) %>% 
             pivot_wider(names_from = TIPO, values_from = n) %>% 
             as_tibble(),
           paste0(
             dir_path,
             "2023_contagem_linha_base/dados/INTERMEDIATE/FINAL_pontos_por_ano.csv"))


# ################################################ #
####          MAPA DOS PONTOS NO TEMPO          ####
# ################################################ #

# shp pontos
pontos_shp <- readRDS(
  paste0(
    dir_path,
    "2023_contagem_linha_base/dados/output/localizacao_ALL.RDS"))

# dados pontos
contagens <- readRDS(
  paste0(
    dir_path, 
    "2023_contagem_linha_base/dados/intermediate/contagens_full_strava.RDS"))

# mapa pontos ano a ano ----
regiao8 <- read_sf(paste0(dir_path, "dados/input/shp_regiao8/SIRGAS_REGIAO8.shp")) %>% 
  st_set_crs(31983) %>%                                     # define o CRS padrão do GEOSAMP
  st_transform(4326) %>%                                    # transforma para WGS 84
  select(NOME) %>%                                          # seleciona colunas de interesse
  rename(REG8 = NOME)                                       # renomeia colunas de interesse

pontos_aux <- pontos_shp %>% 
  right_join(contagens, by = c("ID_PTOS", "TIPO")) %>% 
  filter(!is.na(TOTAL_PICO_STRAVA50) & !is.na(TOTAL_PICO)) %>% 
  select(ID_PTOS, TIPO, DATA, TOTAL_PICO, TOTAL_PICO_STRAVA50) %>% 
  rename(TOTAL_OBS = TOTAL_PICO,
         TOTAL_STRAVA = TOTAL_PICO_STRAVA50) %>% 
  mutate(PERIODO = "pico")

pontos_manha <- pontos_shp %>% 
  right_join(contagens, by = c("ID_PTOS", "TIPO")) %>% 
  filter(!is.na(TOTAL_MANHA_STRAVA50) & !is.na(TOTAL_MANHA)) %>% 
  select(ID_PTOS, TIPO, DATA, TOTAL_MANHA, TOTAL_MANHA_STRAVA50) %>% 
  rename(TOTAL_OBS = TOTAL_MANHA,
         TOTAL_STRAVA = TOTAL_MANHA_STRAVA50) %>% 
  mutate(PERIODO = "manhã")

pontos_tarde <- pontos_shp %>% 
  right_join(contagens, by = c("ID_PTOS", "TIPO")) %>% 
  filter(!is.na(TOTAL_TARDE_STRAVA50) & !is.na(TOTAL_TARDE)) %>% 
  select(ID_PTOS, TIPO, DATA, TOTAL_TARDE, TOTAL_TARDE_STRAVA50) %>% 
  rename(TOTAL_OBS = TOTAL_TARDE,
         TOTAL_STRAVA = TOTAL_TARDE_STRAVA50) %>% 
  mutate(PERIODO = "tarde")

pontos_aux <- pontos_aux %>% 
  rbind(pontos_manha, pontos_tarde)

# i <- "pico"
for (i in c("pico", "manhã", "tarde")) {
  pontos_graph <- pontos_aux %>% 
    mutate(ANO = str_sub(as.character(DATA), start = 1, end = 4),
           TIPO = factor(TIPO, levels = c("manuais", "automáticas", "linha de base"))) %>%
    filter(PERIODO == i & ANO > 2017)
  
  regiao8 %>% 
    ggplot() +
    geom_sf(aes(fill = REG8), color = NA) +
    scale_fill_manual(values = cores, guide = "none") +
    geom_sf(data = pontos_graph, aes(shape = TIPO)) +
    scale_shape_manual(values = c(5, 20, 8))+
    coord_sf() +
    facet_wrap(~ANO) +
    theme_classic() +
    labs(title = paste0("Pontos de contagem - ", i)) +
    theme(legend.position = "bottom",
          legend.direction = "horizontal",
          axis.text = element_blank(),
          axis.ticks = element_blank())
  
  ggsave(plot = last_plot(),
         height = 20,
         width = 18,
         units = "cm",
         paste0(
           dir_path, 
           "2023_contagem_linha_base/figuras/intermediate/pontos_contagem_", i, ".png"))
  
}


# ################################################ #
####              LIMPANDO MEMÓRIA              ####
# ################################################ #
remove(pontos_aux, pontos_df, pontos_graph, pontos_manha, pontos_tarde, regiao8, i)
gc()
